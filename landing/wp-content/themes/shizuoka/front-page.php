<?php get_header(); ?>

<main role="main">
    <!-- section -->
    <section>


        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <!-- article -->
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <!-- content -->
            <picture>
                <source media="(min-width:769px)" srcset="<?php echo get_template_directory_uri(); ?>/img/mainv.png 1x,<?php echo get_template_directory_uri(); ?>/img/mainv.png 2x">
                <source media="(max-width:768px)" srcset="<?php echo get_template_directory_uri(); ?>/img/mainv.png 1x,<?php echo get_template_directory_uri(); ?>/img/mainv.png 2x">
                <img src="<?php echo get_template_directory_uri(); ?>/img/mainv.png" alt="足がつる！こむら返りで目が覚める！それって下肢静脈瘤の初期症状かも！？">
            </picture>

            <picture class="pictureunder">
                <source media="(min-width:769px)" srcset="<?php echo get_template_directory_uri(); ?>/img/kekkanncheck.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/kekkanncheck2.jpg 2x">
                <source media="(max-width:768px)" srcset="<?php echo get_template_directory_uri(); ?>/img/kekkanncheck.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/kekkanncheck2.jpg 2x">
                <img src="<?php echo get_template_directory_uri(); ?>/img/kekkanncheck2.jpg" alt="無料で血管の健康状態をチェックできます">
            </picture>

            <section class="sec01 sec">
                <h2><span class="text-red">この頃、足（ふくらはぎ）がおかしい！なんだかツライ(T T)</span></h2>
                <img src="<?php echo get_template_directory_uri(); ?>/img/picture01.png" srcset="<?php echo get_template_directory_uri(); ?>/img/picture01.png 1x,<?php echo get_template_directory_uri(); ?>/img/picture01@2x.png 2x" alt="足がつる">
                <p>足が重だるい、足がむくんで、パンパンになる、足がつる・こむら返りが痛くて夜、目が覚めてしまう、ふくらはぎが痛い、足の血管が出てきて、痛みや違和感を感じる
などの症状でツライ思いをしていませんか？<br />
それ、下肢静脈瘤（かしじょうみゃくりゅう）のせいかもしれません。そのままにしておくと徐々に症状が進行し、重症化すると手術が必要となります。</p>
            </section>
            <section class="sec02 sec">
                <h2 class="bg-arrow">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/picture02.png" srcset="<?php echo get_template_directory_uri(); ?>/img/picture02.png 1x,<?php echo get_template_directory_uri(); ?>/img/picture02@2x.png 2x" alt="下肢静脈瘤って">下肢静脈瘤ってどんな病気なの？</h2>
                <div class="inner">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/doctor.png" srcset="<?php echo get_template_directory_uri(); ?>/img/doctor.png 1x,<?php echo get_template_directory_uri(); ?>/img/doctor@2x.png 2x" alt="静岡静脈瘤クリニック院長 佐野成一">
                    </div>
                    <div class="text">
                        <p>はじめまして。静岡静脈瘤クリニック院長の佐野成一です。<br /> 
静脈瘤ってご存知ですか？　足の血管が出てきて、ボコボコしていることが多いのですが、それ以外にもこんなことがよくあります。</p>
<p>「何科に行っていいのか分からない。」<br />
「整形外科でレントゲン撮ったけど、特に異常はない。」<br />
「内科で利尿剤を出されたけど、なおらない。」<br />
「静脈瘤はいのちに関わらないから、ストッキング履いておけばいい。」<br />
「知り合いは入院して手術したけど、とても痛かったと言っていた。」</p>
<p>よく外来で聞く言葉なんですが、残念ながら静脈瘤はそのくらい医師にも知られておらず、きちんとみてもらえてないんです。それだけ専門にしている先生はいません。</p>
<p>今、静脈瘤はカテーテルのおかげで、切らずに入院もせず、痛みもなく、10分から1時間で終わり、そのまま歩いて帰れる気軽な日帰り治療になりました。<br /> 
私は東京の下肢静脈瘤クリニックで長年経験を積んできました。しかし、静岡市では下肢静脈瘤を専門にしているクリニックがなかったため、カテーテル治療で治せることがあまり知られていません。</p> 
<p>今まで培ってきた私の知識や手術の技術を地元の皆さんのために活かしたいと思っています。少しでも早く足の悩みから解放され、ストレスのない生活に戻れたら、私は一番嬉しいです。</p>
<p>まず、お気軽に無料血管チェックを受けてみてください。お電話お待ちしています。</p>
                    </div>
                </div>
            </section>
            <section class="sec03 sec">
                <h2 class="bg-arrow">あなたの血管の状態を調べてみませんか？</h2>
                <div class="check-area">
                    <div class="check-box">
                        <p class="title">「患者さんがツライ思いをしないように症状が軽いうちに直せたら！」そんな思いから始めました！</p>
                        <p>「足のむくみがひどくて心配。」「理由はわからないけど、足が重くて疲れる。」「夜、足が冷えて眠れない。」など、足のトラブルでお悩みの方、不安を解消できるように足の血管を無料チェックしてみませんか？</p>

                        <div class="check-title">
                            <p><span class="free">無料</span>血管チェック<span class="jikan">所要時間は約</span>10分</p>
                        </div>
                        <p class="hosoku">※血管チェック（無料）は保険外検査ですが、費用はかかりません。</p>
                        <div class="check-flow">
                            <h3>【血管チェックの流れ】</h3>
                            <ol class="flow">
                                <li>
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/img_01-kekkancheck.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img_01-kekkancheck.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img_01-kekkancheck@2x.jpg 2x" alt="超音波（エコー）による血管チェック">
                                    </div>
                                    <div class="text">
                                        <h4><span>1</span>超音波（エコー）による血管チェック</h4>
                                        <ul>
                                            <li>下肢静脈瘤の有無</li>
                                            <li>血栓の有無</li>
                                            <li>足の血流の状態</li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/img_02-kekkannenrei.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img_02-kekkannenrei.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img_02-kekkannenrei@2x.jpg 2x" alt="血管の状態が測定できます">
                                    </div>
                                    <div class="text">
                                        <h4><span>2</span>血管の状態が測定できます</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/img_04-thermo.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img_04-thermo@2x.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img_04-thermo@2x.jpg 2x" alt="サーモグラフィー（皮ふ温度計）による血流検査">
                                    </div>
                                    <div class="text">
                                        <h4><span>3</span>サーモグラフィー（皮ふ温度計）による血流検査</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/img_03-shinryou.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img_03-shinryou@2x.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img_03-shinryou@2x.jpg 2x" alt="院長による血管を若く保つアドバイス">
                                    </div>
                                    <div class="text">
                                        <h4><span>4</span>院長による血管を若く保つアドバイス</h4>
                                    </div>

                                </li>
                            </ol>
                        </div>
                        <div class="inner yoyaku-area">

                            <div class="image">
								<p class="yoyaku-text"><span>無料</span>血管チェックは下記の日程で行っています！</p>
                                <?php
                        $page_data = get_page_by_path('yoyaku');
                        $page = get_post($page_data);
                        $content = $page -> post_content;
                        echo $content;
                    ?>
                            </div>
                            <div class="banner-area">
                                <div class="yoyaku-banner">
                                    <p>無料血管チェックは<span class="large">電話予約制</span>です</p>
                                    <div class="inner">
                                        <p class="tel"><a href="tel:0542750770">054-275-0770</a></p>
                                        <p>ご予約の際に<br>「無料血管チェック希望」とお伝えください。</p>
                                    </div>
                                </div>
                                <p class="hosoku">※無料静脈瘤検査とリンパ浮腫の診察はネット予約を受け付けておりません。お電話にてご予約ください。</p>
                            </div>
                        </div>
                    </div><!-- /check-box -->
                </div><!-- /check-area -->
            </section>

            <section class="sec04 sec">
                <h2 class="bg-arrow">当院が選ばれる理由</h2>
                <div class="reason-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img-reason01.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img-reason01.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img-reason01@2x.jpg 2x" alt="下肢静脈瘤の外科クリニック" class="ofi">
                    </div>
                    <div class="text">
                        <h3>1.下肢静脈瘤の外科クリニック</h3>
                        <p class="lead">院長が長年経験を積んだ下肢静脈瘤の外科クリニック。</p>
                        <p>東京の下肢静脈瘤を治療する血管外科クリニックで<span class="text-red">長年の経験</span>を積んだ院長が治療にあたります。「分かりやすい治療」をモットーに、患者さんの悩みに耳をかたむけ、足にふれて、不安な気持ちに寄り添います。</p>
                    </div>
                </div>
                <div class="reason-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img-reason02.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img-reason02.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img-reason02@2x.jpg 2x" alt="約15～60分の切らない日帰り治療。" class="ofi">
                    </div>
                    <div class="text">
                        <h3>2.約10～60分の切らない日帰り治療</h3>
                        <p class="lead">治療後歩いて帰ることができ、翌日から日常生活ができます。</p>
                        <p>注射や血管内カテーテル治療によって、約10～60分で治療が終わります。手術後の<span class="text-red">傷跡も目立たない治療</span>です。日帰り治療なので、従来の血管をひきぬく手術のように入院は不要です。</p>
                    </div>
                </div>
                <div class="reason-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img-reason03.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img-reason03.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img-reason03@2x.jpg 2x" alt="健康法検証が使えるので安心して治療ができます。" class="ofi">
                    </div>
                    <div class="text">
                        <h3>3.健康保険証がご利用いただけます</h3>
                        <p class="lead">健康保険証が使えるので安心して治療ができます。</p>
                        <p><span class="text-red">各治療に健康保険証</span>が使用できます。下肢静脈瘤カテーテル治療は医療控除の対象にもなります。また、お支払いにクレジットカードのご利用も可能です。</p>
                    </div>
                </div>
                <div class="reason-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img-reason04.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img-reason04.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img-reason04@2x.jpg 2x" alt="下肢静脈瘤の外科クリニック" class="ofi">
                    </div>
                    <div class="text">
                        <h3>4.駅近だから通院しやすい</h3>
                        <p class="lead">駅から歩いてすぐだから通院しやすいクリニックです。</p>
                        <p><span class="text-red">新静岡駅から徒歩1分、静岡駅北口から徒歩5分</span>の好立地にあるので通院がラクです。</p>
                    </div>
                </div>
                <div class="reason-box">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img-reason05.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img-reason05.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img-reason05@2x.jpg 2x" alt="術後のケアも万全。" class="ofi">
                    </div>
                    <div class="text">
                        <h3>5.術後のケアも万全</h3>
                        <p class="lead">半年後の検診で術後の経過をチェック。</p>
                        <p>半年後の検診で術後の経過をチェック。<span class="text-red">手術後のケア</span>も心掛けています。患者さんの不安が残らないように、スタッフ一同で対応しています。</p>
                    </div>
                </div>

            </section>

            <section class="sec05 sec">
                <h2 class="bg-arrow">安心してご来院いただくために！</h2>
                <p class="lead">静岡静脈瘤クリニックを動画でご紹介します</p>
                <div class="inner">
                    <div class="box"><a href="https://www.youtube.com/watch?v=UxXshPU0sXw"><img src="<?php echo get_template_directory_uri(); ?>/img/thumb_profile.jpg" alt=""></a></div>
                    <div class="box"><a href="https://youtu.be/C8q4jtzaH-A"><img src="<?php echo get_template_directory_uri(); ?>/img/thumb_blood-vessel-check.jpg" alt=""></a></div>
                    <div class="box"><a href="https://youtu.be/3itwOlwfzho"><img src="<?php echo get_template_directory_uri(); ?>/img/thumb_treatment.jpg" alt=""></a></div>
                </div>
            </section>
            <section class="sec06 sec">
                <h2 class="bg-arrow">手術費用</h2>
                <p class="lead text-red">各種クレジットカードのご利用が可能です。支払いの回数がお選び頂けます。</p>
                <p class="hosoku">※カードの種類や患者様とカード会社の契約内容によっては1回払いのみしかご利用できない場合がございますのでご了承下さい。</p>
                <div class="surgery">
                    <table>
                        <tbody>
                            <tr>
                                <th>日帰り手術</th>
                                <th>健康保険1割負担の方</th>
                                <th>健康保険2割負担の方</th>
                                <th>健康保険3割負担の方</th>
                            </tr>
                            <tr>
                                <th>初診(診察料＋超音波検査＋採血)</th>
                                <td>¥3,000 程度</td>
                                <td>¥5,000 程度</td>
                                <td>¥7,000 程度</td>
                            </tr>
                            <tr>
                                <th style="border-bottom:1px solid #fff;">高周波カテーテル治療</th>
                                <td style="border-bottom:1px solid #fff;"><span>医療費上限</span>¥18,000</td>
                                <td style="border-bottom:1px solid #fff;"><span>医療費上限</span>¥18,000</td>
                                <td style="border-bottom:1px solid #fff;">ー</td>
                            </tr>
                            <tr>
                                <th style="border-bottom:1px solid #fff;">片足（医療費＋自費）</th>
                                <td style="border-bottom:1px solid #fff;">¥21,000</td>
                                <td style="border-bottom:1px solid #fff;">¥28,000</td>
                                <td style="border-bottom:1px solid #fff;">¥46,000</td>
                            </tr>
                            <tr>
                                <th>両足（医療費＋自費）</th>
                                <td>¥28,000</td>
                                <td>¥28,000</td>
                                <td>¥76,000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="text-green">■他手術費については<a href="https://shizuoka.varix.co.jp/price/" target="_blank">コチラ</a>をクリック</p>
                <ul class="ul-kome hosoku">
                    <li>上記費用は税別となります。</li>
                    <li>弾性ストッキングを当日処方される方は上記金額に含まれています。<br>弾性ソックス･･･￥6,000(税別)  弾性ストッキング･･･7,000円(税別)</li>
                </ul>
            </section>
            <section class="sec07 sec">
                <h2 class="bg-arrow">足がつる・重だるい・むくむ・冷える！<br>そんな症状を改善したい方は</h2>
                <a href="https://medical.apokul.jp/web/47/reservations/add?_ga=2.164526719.1792020195.1599456237-1345456217.1594365270">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/banner-yoyaku2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/banner-yoyaku2.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/banner-yoyaku2@2x.jpg 2x" alt="ネット予約（初診）はこちらから" class="banner">
                </a>
            </section>
            <section class="sec08 sec">
                <div class="inner">
                    <div class="text">
                        <h2>■診療日・時間</h2>
                        <dl>
                            <dt>診療日</dt>
                            <dd>月～金（土・日・祝休）</dd>
                            <dt>診療受付</dt>
                            <dd>午前9時～午前11時30分・午後1時～午後4時30分</dd>
                        </dl>
                        <ul class="ul-kome hosoku">
                            <li>午後は手術の時間になります。</li>
                            <li>リンパ浮腫外来は第2、第4金曜日の15:00〜16:00の診察になります。</li>
                        </ul>
<dl class="tel">
<dt>【ご予約】</dt>
<dd><span><a href="tel:0542750770">054-275-0770</a></span></dd>
</dl>
                        <h2>■所在地</h2>
                        <p>〒420-0858 静岡県静岡市葵区伝馬町８−１サンローゼビル２階</p>
                        <p>※駐車場はございません。公共交通機関をご利用ください。</p>
                    </div>
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/img_information.jpg" srcset="<?php echo get_template_directory_uri(); ?>/img/img_information.jpg 1x,<?php echo get_template_directory_uri(); ?>/img/img_information.jpg 2x" alt="診察受付">
                    </div>

                </div>
                <div class="gmap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3269.268649216899!2d138.38493005150949!3d34.97493498026851!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601a49f7dae2ed95%3A0x4f906a1e6a2177a3!2z44CSNDIwLTA4NTgg6Z2Z5bKh55yM6Z2Z5bKh5biC6JG15Yy65Lyd6aas55S677yY4oiS77yR!5e0!3m2!1sja!2sjp!4v1599524941826!5m2!1sja!2sjp" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
            </section>
            <!-- /content -->
            <!--            <?php the_content(); ?>-->

            <?php comments_template( '', true ); // Remove if you don't want comments ?>

            <br class="clear">

        </article>
        <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

        <!-- article -->
        <article>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

        </article>
        <!-- /article -->

        <?php endif; ?>

    </section>
    <!-- /section -->
</main>


<?php get_footer(); ?>
