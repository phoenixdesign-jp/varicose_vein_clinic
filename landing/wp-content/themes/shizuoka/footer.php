			<!-- footer -->
			<footer class="footer" role="contentinfo">

			    <!-- copyright -->
			    <p class="copyright">&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?></p>
			    <!-- /copyright -->

			</footer>
			<!-- /footer -->

			</div>
			<!-- /wrapper -->
        <div class="mobile">
            <ul>
                        <li class="tel"><a href="tel:0542750770"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone-alt" class="svg-inline--fa fa-phone-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path fill="currentColor" d="M497.39 361.8l-112-48a24 24 0 0 0-28 6.9l-49.6 60.6A370.66 370.66 0 0 1 130.6 204.11l60.6-49.6a23.94 23.94 0 0 0 6.9-28l-48-112A24.16 24.16 0 0 0 122.6.61l-104 24A24 24 0 0 0 0 48c0 256.5 207.9 464 464 464a24 24 0 0 0 23.4-18.6l24-104a24.29 24.29 0 0 0-14.01-27.6z"></path>
							</svg>054-275-0770</a></li>
                        <li class="mail"><a href="https://medical.apokul.jp/web/47/reservations/add?_ga=2.164526719.1792020195.1599456237-1345456217.1594365270" target="_blank">ご予約はこちら<br />24H受付中<span><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path>
                                    </svg></span></a></li>
                    </ul>
        </div>
			<?php wp_footer(); ?>

			<!-- analytics -->
			<script>
			    (function(f, i, r, e, s, h, l) {
			        i['GoogleAnalyticsObject'] = s;
			        f[s] = f[s] || function() {
			            (f[s].q = f[s].q || []).push(arguments)
			        }, f[s].l = 1 * new Date();
			        h = i.createElement(r),
			            l = i.getElementsByTagName(r)[0];
			        h.async = 1;
			        h.src = e;
			        l.parentNode.insertBefore(h, l)
			    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
			    ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
			    ga('send', 'pageview');

			</script>

			</body>

			</html>
