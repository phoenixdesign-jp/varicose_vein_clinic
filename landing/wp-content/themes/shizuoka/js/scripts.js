(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

    });

})(jQuery, this);

// sp menu
jQuery(function () {
    jQuery("#js-toggle").click(function () {
        jQuery("#js-toggle-icon").toggleClass("open");
        jQuery("#js-menu-sp").slideToggle();
    })
})

// sp sub-menu

jQuery(function () {
    jQuery("#menu-sp-menu .sub-menu").after("<span class='acc-btn'></span>");
    jQuery("#menu-sp-menu .sub-menu").hide();
    jQuery(".acc-btn").on("click", function () {
        jQuery(this).prev("ul").slideToggle(300);
        jQuery(this).toggleClass("active");
    });
});


// adobe font
//(function (d) {
//    var config = {
//            kitId: 'vae8dja',
//            scriptTimeout: 3000,
//            async: true
//        },
//        h = d.documentElement,
//        t = setTimeout(function () {
//            h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
//        }, config.scriptTimeout),
//        tk = d.createElement("script"),
//        f = false,
//        s = d.getElementsByTagName("script")[0],
//        a;
//    h.className += " wf-loading";
//    tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
//    tk.async = true;
//    tk.onload = tk.onreadystatechange = function () {
//        a = this.readyState;
//        if (f || a && a != "complete" && a != "loaded") return;
//        f = true;
//        clearTimeout(t);
//        try {
//            Typekit.load(config)
//        } catch (e) {}
//    };
//    s.parentNode.insertBefore(tk, s)
//})(document);
