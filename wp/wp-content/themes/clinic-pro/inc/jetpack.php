<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package clinic-pro
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function clinic_pro_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'clinic_pro_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function clinic_pro_jetpack_setup
add_action( 'after_setup_theme', 'clinic_pro_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function clinic_pro_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function clinic_pro_infinite_scroll_render
