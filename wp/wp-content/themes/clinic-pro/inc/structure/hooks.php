<?php

/**
 * clinic hooks
 *
 * @package clinic-pro
 */
add_action( 'clinic_pro_header', 'clinic_pro_branding', 20 );
add_action( 'clinic_pro_header_navigation', 'clinic_pro_navigation_primary', 20 );
