<?php

//Color fields
$clinic_pro_default_options = clinic_pro_get_option_defaults();

// General colors
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_general_swatch',
	'label'			 => __( 'Primary Swatch Color', 'clinic-pro' ),
	'description'	 => __( 'Select the primary color of your brand.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_general',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_general_swatch'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '
				.advanced-sidebar-menu ul > li > a:hover,
				.advanced-sidebar-menu ul li.current_page_item > a, .advanced-sidebar-menu ul li.current_page_item > a:hover, 
				body .advanced-sidebar-menu ul li.current_page_item > a, body .advanced-sidebar-menu ul li.current_page_item > a:hover,
				.ccfw-header-details .widget.widget_text i,
				.blog-menu > li:hover > a,
            	.blog-menu > li.current_page_item > a, 
            	.blog-menu > li.current_page_item:hover > a, 
            	.blog-menu > .has-children li a:hover,
            	.blog-menu > li.current_page_ancestor > a, 
            	.blog-menu > li.current_page_ancestor:hover > a, 
            	.blog-menu > li.current_page_parent > a, 
            	.blog-menu > li.current_page_parent:hover > a,
            	.blog-menu > li > a:hover,
            	.ccfw-blog-pagination ul li.active a,
            	.ccfw-blog-pagination ul li a:hover,
            	.widget_product_categories ul li.current-cat > a,
				.widget_product_categories ul li.current-cat-parent.cat-parent > a,
				.widget_product_categories ul li a:hover
            	',
			'property'	 => 'color'
		),
		array(
			'element'	 => '
            	.content-area p a.more-link,
            	.post-navigation span.meta-nav,
            	.mc4wp-form input[type="submit"],
            	.content-area input[type="submit"],
            	.content-area input[type="button"],
            	.blog-menu > li > a:before,
            	.ccfw-first-footer-wrapper .textwidget li:before,
				#ccfw-back-to-top,
				.blog-menu > li > a strong,
				.woocommerce button.button.alt,
				.woocommerce button.button.alt:hover,
				.woocommerce a.button.alt,
				.woocommerce a.button.alt:hover,
				.woocommerce input.button.alt,
				.woocommerce input.button.alt:hover,
				.woocommerce input.button:hover,
				.woocommerce table.my_account_orders .button,
				.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content
            	',
			'property'	 => 'background-color'
		),
		array(
			'element'	 => '
            	.ccfw-blog-pagination ul li a:hover,
            	.ccfw-blog-pagination ul li.active a,
				body.woocommerce-page ul.products li.product a:hover img
            	',
			'property'	 => 'border-color'
		),
		array(
			'element'	 => '
            	.elementor-widget-tabs .elementor-tabs .elementor-tabs-wrapper .elementor-tab-title.active
            	',
			'property'	 => 'border-top-color'
		),
		array(
			'element'	 => '
			.sticky,
			.advanced-sidebar-menu ul > li > a:hover,
			.advanced-sidebar-menu ul li.current_page_item.has_children a,
			.advanced-sidebar-menu ul li.current_page_item a, .advanced-sidebar-menu ul li.current_page_parent a, 
			.sidebar.right-sidebar .advanced-sidebar-menu ul > li > a:hover,
			.widget_product_categories ul li.current-cat > a,
			.widget_product_categories ul li.current-cat-parent.cat-parent > a,
			.widget_product_categories ul.children li a,
			.widget_product_categories ul li a:hover',
			'property'	 => 'border-left-color'
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '
				.advanced-sidebar-menu ul > li > a:hover,
				.advanced-sidebar-menu ul li.current_page_item > a, 
				.advanced-sidebar-menu ul li.current_page_item > a:hover, 
				body .advanced-sidebar-menu ul li.current_page_item > a, 
				body .advanced-sidebar-menu ul li.current_page_item > a:hover,
				.ccfw-header-details .widget.widget_text i,
				.blog-menu > li:hover > a,
				.blog-menu > li.current_page_item > a, 
            	.blog-menu > li.current_page_item:hover > a, 
            	.blog-menu > li > a:before,
            	.blog-menu > .has-children li a:hover,
            	.blog-menu > li.current_page_ancestor > a, 
            	.blog-menu > li.current_page_ancestor:hover > a, 
            	.blog-menu > li.current_page_parent > a, 
            	.blog-menu > li.current_page_parent:hover > a,
            	.blog-menu ul.children li:hover > a,  
            	.blog-menu li ul li a:hover,
            	.advanced-sidebar-menu ul li a:hover,
				.sticky-header .blog-menu > li > a:hover,
				.blog-menu > li > a:hover,
				.ccfw-blog-pagination ul li.active a,
				.ccfw-blog-pagination ul li a:hover,
				.widget_product_categories ul li.current-cat > a,
				.widget_product_categories ul li.current-cat-parent.cat-parent > a,
				.widget_product_categories ul li a:hover
				',
			'property'	 => 'color'
		),
		array(
			'element'	 => '
            	.content-area p a.more-link,
            	.post-navigation span.meta-nav,
            	.mc4wp-form input[type="submit"],
            	.content-area input[type="submit"],
            	.content-area input[type="button"],
            	.blog-menu > li > a:before,
            	.ccfw-first-footer-wrapper .textwidget li:before,
            	#ccfw-back-to-top,
            	.blog-menu > li > a strong,
				.woocommerce button.button.alt,
				.woocommerce button.button.alt:hover,
				.woocommerce a.button.alt,
				.woocommerce a.button.alt:hover,
				.woocommerce input.button.alt,
				.woocommerce input.button.alt:hover,
				.woocommerce input.button:hover,
				.woocommerce table.my_account_orders .button,
				.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,
				.widget_product_categories ul li.current-cat > a
				',
			'property'	 => 'background-color'
		),
		array(
			'element'	 => '
            	.ccfw-blog-pagination ul li a:hover,
            	.ccfw-blog-pagination ul li.active a,
				body.woocommerce-page ul.products li.product a:hover img',
			'property'	 => 'border-color'
		),
		array(
			'element'	 => '
            	.elementor-widget-tabs .elementor-tabs .elementor-tabs-wrapper .elementor-tab-title.active
            	',
			'property'	 => 'border-top-color'
		),
		array(
			'element'	 => '
			.sticky,
			.advanced-sidebar-menu ul > li > a:hover,
			.advanced-sidebar-menu ul li.current_page_item.has_children a,
			.advanced-sidebar-menu ul li.current_page_item a, .advanced-sidebar-menu ul li.current_page_parent a, 
			.sidebar.right-sidebar .advanced-sidebar-menu ul > li > a:hover,
			.widget_product_categories ul li.current-cat > a,
			.widget_product_categories ul li.current-cat-parent.cat-parent > a,
			.widget_product_categories ul.children li a,
			.widget_product_categories ul li a:hover',
			'property'	 => 'border-left-color'
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_general_swatch_text',
	'label'			 => esc_html__( 'Text on the primary swatch', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select the color of text on the primary swatch. (Usually white or black)', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_general',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_general_swatch_text'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '
            .content-area p a.more-link,
            .content-area input[type="submit"], 
            .content-area input[type="reset"], 
            .content-area input[type="button"], 
            .jetpack_subscription_widget input[type="submit"], 
            .blog-menu > li > a strong,
            .woocommerce button.button.alt,
            .woocommerce button.button.alt:hover,
            .woocommerce a.button.alt,
			.woocommerce a.button.alt:hover,
			.woocommerce input.button.alt,
			.woocommerce input.button.alt:hover,
			.woocommerce input.button:hover,
			.woocommerce table.my_account_orders .button
            ',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '
            .content-area p a.more-link,
            .content-area input[type="submit"], 
            .content-area input[type="reset"], 
            .content-area input[type="button"], 
            .jetpack_subscription_widget input[type="submit"], 
            .blog-menu > li > a strong,
            .woocommerce button.button.alt,
            .woocommerce button.button.alt:hover,
            .woocommerce a.button.alt,
			.woocommerce a.button.alt:hover,
			.woocommerce input.button.alt,
			.woocommerce input.button.alt:hover,
			.woocommerce input.button:hover,
			.woocommerce table.my_account_orders .button
            ',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_general_links',
	'label'			 => esc_html__( 'General links', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your general links color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_general',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_general_links'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => 'a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => 'a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_general_links_hover',
	'label'			 => esc_html__( 'General link hover', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your general link hover color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_general',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_general_links_hover'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => 'a:hover',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => 'a:hover',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_wrapper_background',
	'label'			 => esc_html__( 'Wrapper Background Color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your wrapper background color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_container',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_wrapper_background'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '#wrapper',
			'property'	 => 'background-color',
		)
	)
) );

// Top bar colors
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_topbar_background',
	'label'			 => esc_html__( 'Top bar background', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your top bar background color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_topbar',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_topbar_background'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-header-topbar',
			'property'	 => 'background-color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-header-topbar',
			'function'	 => 'css',
			'property'	 => 'background-color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_topbar_color',
	'label'			 => esc_html__( 'Top bar text', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your top bar text color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_topbar',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_topbar_color'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-header-topbar, .ccfw-header-topbar a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-header-topbar, .ccfw-header-topbar a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

// Header colors
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_header_background',
	'label'			 => esc_html__( 'Header background', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your header background color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_header',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_header_background'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-header-main',
			'property'	 => 'background-color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-header-main',
			'function'	 => 'css',
			'property'	 => 'background-color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_header_site_title',
	'label'			 => esc_html__( 'Site title', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your site title color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_header',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_header_site_title'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-site-title a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-site-title a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_header_site_desc',
	'label'			 => esc_html__( 'Site description', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your site description color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_header',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_header_site_desc'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-site-description',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-site-description',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_header_text',
	'label'			 => esc_html__( 'Header text', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your header text color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_header',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_header_text'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-header-main, .ccfw-header-main h4, .ccfw-header-main a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-header-main, .ccfw-header-main h4, .ccfw-header-main a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

// Page heading colors
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_heading_gradient_left',
	'label'			 => esc_html__( 'Page heading gradient left color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select the left gradient color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_pageheading',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_heading_gradient_left'],
	'priority'		 => 10,
	'transport'		 => 'postMessage',
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_heading_gradient_right',
	'label'			 => esc_html__( 'Page heading gradient right color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select the right gradient color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_pageheading',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_heading_gradient_right'],
	'priority'		 => 10,
	'transport'		 => 'postMessage',
) );

// Footer colors
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_first_bg',
	'label'			 => esc_html__( 'First footer background color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your first footer background color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_first_bg'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper',
			'property'	 => 'background-color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper',
			'function'	 => 'css',
			'property'	 => 'background-color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_first_text',
	'label'			 => esc_html__( 'First footer text color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your first footer text color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_first_text'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper, .ccfw-first-footer-wrapper h4, .ccfw-first-footer-wrapper p',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper, .ccfw-first-footer-wrapper h4, .ccfw-first-footer-wrapper p',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_first_links',
	'label'			 => esc_html__( 'First footer links', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your first footer links color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_first_links'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_first_link_hover',
	'label'			 => esc_html__( 'First footer link hover', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your first footer link hover color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_first_link_hover'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper a:hover',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-first-footer-wrapper a:hover',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );


clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_copyright_bg',
	'label'			 => esc_html__( 'Copyright Background Color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your copyright background color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_copyright_bg'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-last-footer',
			'property'	 => 'background-color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-last-footer',
			'function'	 => 'css',
			'property'	 => 'background-color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_copyright_text',
	'label'			 => esc_html__( 'Copyright Text Color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your copyright text color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_copyright_text'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-last-footer',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-last-footer',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );

clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'color',
	'settings'		 => 'clinic_pro_color_footer_copyright_links',
	'label'			 => esc_html__( 'Copyright Links Color', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select your copyright links color.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_color_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_color_footer_copyright_links'],
	'priority'		 => 10,
	'output'		 => array(
		array(
			'element'	 => '.ccfw-last-footer a',
			'property'	 => 'color',
		)
	),
	'transport'		 => 'postMessage',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-last-footer a',
			'function'	 => 'css',
			'property'	 => 'color',
		),
	)
) );
