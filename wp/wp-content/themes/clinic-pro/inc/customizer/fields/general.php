<?php

// General fields
$clinic_pro_default_options = clinic_pro_get_option_defaults();

// Header Logo Height
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'slider',
	'settings'		 => 'clinic_pro_logo_height',
	'label'			 => esc_html__( 'Logo Height', 'clinic-pro' ),
	'description'	 => esc_html__( 'Adjust the height of your logo in pixels.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_section_general_logo_height',
	'default'  		=> 60,
	'priority' 		=> 1,
	'choices'  		=> array(
		'min'  		=> 0,
		'max'  		=> 100,
		'step' 		=> 1,
	),
	'output' 		=> array(
		array(
			'element'  => '.ccfw-site-logo img',
			'property' => 'height',
			'units'    => 'px',
		),
	),
) );