<?php

// Typography fields
// Main body fields
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'		 => 'typography',
	'settings'	 => 'clinic_pro_typography_mainbody_fontfamily',
	'label'		 => esc_html__( 'Font settings', 'clinic-pro' ),
	'section'	 => 'clinic_pro_typography_section_mainbody',
	'default'	 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '18px',
		'line-height'	 => '1.72',
		'letter-spacing' => '0',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#555',
		'text-transform' => 'none'
	),
	'priority'	 => 10,
	'output'	 => array(
		array(
			'element'	 => 'body, input, select, textarea, button, .elementor-widget-tabs .elementor-tab-title > span, body .elementor-widget-button .elementor-button',
			'property'	 => 'font-family',
		),
	),
) );

// Main Menu
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_mainmenu_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_mainmenu',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'font-weight'	 => '300',
		'font-size'		 => '17px',
		'letter-spacing' => '0px',
		'subsets'		 => array( 'latin-ext' ),
		'text-transform' => 'none'
	),
	'priority'		 => 20,
	'output'		 => array(
		array(
			'element'	 => '.blog-menu > li > a',
			'property'	 => 'font-family',
		)
	)
) );

// h1
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h1_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h1',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'font-weight'	 => '300',
		'font-size'		 => '38px',
		'line-height'	 => '1.2',
		'letter-spacing' => '0',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none'
	),
	'priority'		 => 20,
	'output'		 => array(
		array(
			'element'	 => 'h1, .entry-header h1',
			'property'	 => 'font-family',
		)
	)
) );

// h2
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h2_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h2',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '34px',
		'line-height'	 => '1.45',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none',
		'letter-spacing' => '-0.3px',
	),
	'priority'		 => 30,
	'output'		 => array(
		array(
			'element'	 => 'h2',
			'property'	 => 'font-family',
		)
	)
) );


// h3
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h3_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h3',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '28px',
		'line-height'	 => '1.45',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none'
	),
	'priority'		 => 40,
	'output'		 => array(
		array(
			'element'	 => 'h3',
			'property'	 => 'font-family',
		)
	)
) );


// h4
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h4_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h4',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '24px',
		'line-height'	 => '1.5',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none'
	),
	'priority'		 => 50,
	'output'		 => array(
		array(
			'element'	 => 'h4',
			'property'	 => 'font-family',
		)
	)
) );


// h5
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h5_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h5',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '18px',
		'line-height'	 => '1.5',
		'letter-spacing' => '0',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none'
	),
	'priority'		 => 60,
	'output'		 => array(
		array(
			'element'	 => 'h5',
			'property'	 => 'font-family',
		)
	)
) );


// h6
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'typography',
	'settings'		 => 'clinic_pro_typography_h6_fontfamily',
	'label'			 => esc_html__( 'Font Settings', 'clinic-pro' ),
	'description'	 => esc_html__( 'Select which font you would like to use', 'clinic-pro' ),
	'section'		 => 'clinic_pro_typography_section_headings_h6',
	'default'		 => array(
		'font-family'	 => 'Open Sans',
		'variant'		 => '300',
		'font-size'		 => '16px',
		'line-height'	 => '1.5',
		'letter-spacing' => '0',
		'subsets'		 => array( 'latin-ext' ),
		'color'			 => '#333333',
		'text-transform' => 'none'
	),
	'priority'		 => 70,
	'output'		 => array(
		array(
			'element'	 => 'h6',
			'property'	 => 'font-family',
		)
	)
) );
