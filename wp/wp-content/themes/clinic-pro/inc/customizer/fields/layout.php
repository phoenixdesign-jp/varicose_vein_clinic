<?php

// Layout fields
$clinic_pro_default_options = clinic_pro_get_option_defaults();

// Top bar fields
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_topbar',
	'label'			 => esc_html__( 'Top bar', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide the Top bar?', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_topbar',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_topbar'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'show'	 => esc_html__( 'Show', 'clinic-pro' ),
		'hide'	 => esc_html__( 'Hide', 'clinic-pro' ),
	),
) );

// Header details on mobile.
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_header_details_visibility',
	'label'			 => esc_html__( 'Header details visibility', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show the header details on mobile.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_header_details',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_header_details_visibility'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'show'	 => esc_html__( 'Show', 'clinic-pro' ),
		'hide'	 => esc_html__( 'Hide', 'clinic-pro' ),
	),
) );

// Header Height.
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'slider',
	'settings'		 => 'clinic_pro_header_height',
	'label'			 => esc_html__( 'Header Height', 'clinic-pro' ),
	'description'	 => esc_html__( 'Adjust the header height', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_header_height',
	'default'		 => 118,
	'priority'		 => 1,
	'choices'		 => array(
		'min'	 => 0,
		'max'	 => 250,
		'step'	 => 1,
	),
	'output'		 => array(
		array(
			'element'	 => '.ccfw-site-logo a, .ccfw-header-details-right',
			'property'	 => 'line-height',
			'units'		 => 'px',
		),
		array(
			'element'	 => '.ccfw-header-details, .ccfw-header-details-right',
			'property'	 => 'height',
			'units'		 => 'px',
		),
	),
) );

// Sticky header.
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_sticky_header',
	'label'			 => esc_html__( 'Enable the sticky header.', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide the sticky header upon scroll.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_sticky',
	'default'		 => $clinic_pro_default_options['clinic_pro_sticky_header'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'show'	 => esc_html__( 'Show', 'clinic-pro' ),
		'hide'	 => esc_html__( 'Hide', 'clinic-pro' ),
	),
) );

// Page sidebar
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_page_listing_sidebar',
	'label'			 => esc_html__( 'Page sidebar', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide a Page sidebar?', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_page_sidebar',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_page_listing_sidebar'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'left'	 => esc_html__( 'Left', 'clinic-pro' ),
		'right'	 => esc_html__( 'Right', 'clinic-pro' ),
		'none'	 => esc_html__( 'None', 'clinic-pro' ),
	),
) );

// Blog sidebar
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_blog_listing_sidebar',
	'label'			 => esc_html__( 'Blog sidebar', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide a Blog sidebar?', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_blog_sidebar',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_blog_listing_sidebar'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'left'	 => esc_html__( 'Left', 'clinic-pro' ),
		'right'	 => esc_html__( 'Right', 'clinic-pro' ),
		'none'	 => esc_html__( 'None', 'clinic-pro' ),
	),
) );

// Listings Excerpt or Read More
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_post_item_display',
	'label'			 => esc_html__( 'Listings Excerpts', 'clinic-pro' ),
	'description'	 => esc_html__( 'Display a snippet of text beneath each blog listing', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_blog_excerpt',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_post_item_display'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'more'	 		=> esc_html__( 'Use the more tag', 'clinic-pro' ),
		'excerpt'	 	=> esc_html__( 'Use the automatic excerpt', 'clinic-pro' ),
	),
) );

// Footer fields
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_layout_first_footer',
	'label'			 => esc_html__( 'Show Footer?', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide the Footer?', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_layout_first_footer'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'show'	 => esc_html__( 'Show', 'clinic-pro' ),
		'hide'	 => esc_html__( 'Hide', 'clinic-pro' ),
	),
) );

// Footer Back to Top.
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'select',
	'settings'		 => 'clinic_pro_backtotop',
	'label'			 => esc_html__( 'Show Back to Top?', 'clinic-pro' ),
	'description'	 => esc_html__( 'Show or hide the back to top arrow.', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_footer',
	'default'		 => $clinic_pro_default_options['clinic_pro_backtotop'],
	'priority'		 => 10,
	'transport'		 => 'refresh',
	'choices'		 => array(
		'show'	 => esc_html__( 'Show', 'clinic-pro' ),
		'hide'	 => esc_html__( 'Hide', 'clinic-pro' ),
	),
) );

// Footer Copyright
clinic_pro_Kirki::add_field( 'clinic_pro_config', array(
	'type'			 => 'textarea',
	'settings'		 => 'clinic_pro_footer_copyright_text',
	'label'			 => esc_html__( 'Footer Copyright text', 'clinic-pro' ),
	'description'	 => esc_html__( 'Manage your Footer Copyright text', 'clinic-pro' ),
	'section'		 => 'clinic_pro_layout_section_footer_copyright',
	'default'		 => $clinic_pro_default_options['clinic_pro_footer_copyright_text'], 
	'priority'		 => 10,
	'transport'		 => 'auto',
	'js_vars'		 => array(
		array(
			'element'	 => '.ccfw-footer-msg',
			'function'	 => 'html',
		),
	)
) );