<?php

// Help fields
$clinic_pro_default_options = clinic_pro_get_option_defaults();

clinic_pro_Kirki::add_field( 'my_config', array(
	'type'		 => 'custom',
	'settings'	 => 'clinic_pro_help_pro_cta',
	'label'		 => __( 'Premium Customer Support', 'clinic-pro' ),
	'section'	 => 'clinic_pro_section_help_help',
	'default'	 => '<div style="padding: 30px;background-color: #333; color: #fff;">' . esc_html__( 'Clinic is provided as is. Clinic Pro provides tons of additional features, including premium customer support. Check it out on https://createandcode.com', 'clinic-pro' ) . '</div>',
	'priority'	 => 10,
) );
