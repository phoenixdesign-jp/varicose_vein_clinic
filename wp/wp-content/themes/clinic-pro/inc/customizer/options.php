<?php

// Some wrappers for theme mods/options and their defaults
//Set sensible defaults
require_once get_template_directory() . '/inc/customizer/defaults.php';

if ( !function_exists( 'clinic_pro_get_option' ) ) {

	function clinic_pro_get_option( $key ) {
		$clinic_pro_options	 = clinic_pro_get_options();
		$clinic_pro_option	 = get_theme_mod( $key, $clinic_pro_options[$key] );
		return $clinic_pro_option;
	}

}

if ( !function_exists( 'clinic_pro_get_options' ) ) {

	// Get theme option defaults
	function clinic_pro_get_options() {
		return wp_parse_args(
		get_theme_mods(), clinic_pro_get_option_defaults()
		);
	}

}