<?php

function clinic_pro_kirki_panels( $wp_customize ) {

	$wp_customize->add_panel( 'clinic_pro_panel_general', array(
		'priority'		 => 10,
		'title'			 => __( 'General Settings', 'clinic-pro' ),
		'description'	 => __( 'Manage general theme settings', 'clinic-pro' ),
	) );
	$wp_customize->add_panel( 'clinic_pro_panel_colors', array(
		'priority'		 => 10,
		'title'			 => __( 'Color', 'clinic-pro' ),
		'description'	 => __( 'Manage theme colors', 'clinic-pro' ),
	) );
	$wp_customize->add_panel( 'clinic_pro_panel_typography', array(
		'priority'		 => 10,
		'title'			 => __( 'Typography', 'clinic-pro' ),
		'description'	 => __( 'Manage theme typography', 'clinic-pro' ),
	) );
	$wp_customize->add_panel( 'clinic_pro_panel_layout', array(
		'priority'		 => 10,
		'title'			 => __( 'Layout', 'clinic-pro' ),
		'description'	 => __( 'Manage theme header, footer and more', 'clinic-pro' ),
	) );
	$wp_customize->add_panel( 'clinic_pro_panel_blog', array(
		'priority'		 => 10,
		'title'			 => __( 'Blog', 'clinic-pro' ),
		'description'	 => __( 'Manage blog settings', 'clinic-pro' ),
	) );
}

add_action( 'customize_register', 'clinic_pro_kirki_panels' );
