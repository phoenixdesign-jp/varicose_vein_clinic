<?php

if ( !function_exists( 'clinic_pro_get_option_defaults' ) ) {

	// Set sane theme option defaults - hattip - https://make.wordpress.org/themes/2014/07/09/using-sane-defaults-in-themes/
	function clinic_pro_get_option_defaults() {
		$defaults = array(
			// Layout
			'clinic_pro_layout_topbar'						 => 'show',
			'clinic_pro_layout_header_details_visibility'	 => 'show',
			'clinic_pro_sticky_header'						 => 'show',
			'clinic_pro_layout_first_footer'				 => 'show',
			'clinic_pro_layout_second_footer'				 => 'show',
			'clinic_pro_layout_post_item_display'			 => 'more',
			'clinic_pro_layout_blog_listing_sidebar'		 => 'left',
			'clinic_pro_layout_page_listing_sidebar'		 => 'left',
			'clinic_pro_layout_blog_listing_featured_img'	 => 'above',
			'clinic_pro_footer_copyright_text'				 => 'Clinic Pro by Create and Code.',
			'clinic_pro_backtotop'							 => 'show',
			// Color
			'clinic_pro_color_general_swatch'				 => '#07a7e3',
			'clinic_pro_color_general_swatch_text'			 => '#ffffff',
			'clinic_pro_color_general_text'					 => '#343434',
			'clinic_pro_color_overall_background'			 => '#333',
			'clinic_pro_color_wrapper_background'			 => '#fff',
			'clinic_pro_color_topbar_background'			 => '#eaf6fa',
			'clinic_pro_color_topbar_color'					 => '#043b4e',
			'clinic_pro_body_background_color'				 => '#000',
			'clinic_pro_color_general_links'				 => '#07a7e3',
			'clinic_pro_color_general_links_hover'			 => '#000000',
			'clinic_pro_color_section_topbar'				 => '#fff',
			'clinic_pro_color_header_background'			 => '#fff',
			'clinic_pro_color_header_site_title'			 => '#333',
			'clinic_pro_color_header_site_desc'				 => '#333',
			'clinic_pro_color_header_text'					 => '#333',
			'clinic_pro_color_heading_gradient_left'		 => '#07a7e3',
			'clinic_pro_color_heading_gradient_right'		 => '#37d6c0',
			'clinic_pro_color_footer_first_bg'				 => '#444f5d',
			'clinic_pro_color_footer_first_text'			 => '#fff',
			'clinic_pro_color_footer_first_links'			 => '#fff',
			'clinic_pro_color_footer_first_link_hover'		 => '#d8e4f2',
			'clinic_pro_color_footer_copyright_bg'			 => '#37414e',
			'clinic_pro_color_footer_copyright_text'		 => '#d8e4f2',
			'clinic_pro_color_footer_copyright_links'		 => '#fff',
		);

		return apply_filters( 'clinic_pro_get_option_defaults', $defaults );
	}

}