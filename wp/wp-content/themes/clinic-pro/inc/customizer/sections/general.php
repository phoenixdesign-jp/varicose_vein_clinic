<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Create sections using the WordPress Customizer API.
 */
function clinic_pro_kirki_section_general( $wp_customize ) {

	/**
	 * Add sections
	 */
	//General
	$wp_customize->add_section( 'clinic_pro_section_general_logo', array(
		'title'			 => __( 'Logo', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_general',
		'priority'		 => 10,
		'description'	 => __( 'Upload your logo', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_section_general_logo_height', array(
		'title'			 => __( 'Logo Height', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_general',
		'priority'		 => 10,
		'description'	 => __( 'Modify the height of your logo', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_section_general_footer', array(
		'title'			 => __( 'Footer Copyright Text', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_general',
		'priority'		 => 10,
		'description'	 => __( 'Manage your Footer copyright text', 'clinic-pro' ),
	) );
}

add_action( 'customize_register', 'clinic_pro_kirki_section_general' );
