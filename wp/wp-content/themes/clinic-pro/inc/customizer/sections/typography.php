<?php

/**
 * Create sections using the WordPress Customizer API.
 */
function clinic_pro_kirki_section_typography( $wp_customize ) {

	/**
	 * Add sections
	 */

	//Typography
	$wp_customize->add_section( 'clinic_pro_typography_section_mainbody', array(
		'title'       => __( 'Main Body', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the main body text', 'clinic-pro' ),
	) );

	//Main Menu
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_mainmenu', array(
		'title'       => __( 'Main Menu', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Main Menu', 'clinic-pro' ),
	) );

	//Heading One
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h1', array(
		'title'       => __( 'Heading One', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading One', 'clinic-pro' ),
	) );

	//Heading Two
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h2', array(
		'title'       => __( 'Heading Two', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading Two', 'clinic-pro' ),
	) );

	//Heading Three
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h3', array(
		'title'       => __( 'Heading Three', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading Three', 'clinic-pro' ),
	) );

	//Heading Four
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h4', array(
		'title'       => __( 'Heading Four', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading Four', 'clinic-pro' ),
	) );

	//Heading Five
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h5', array(
		'title'       => __( 'Heading Five', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading Five', 'clinic-pro' ),
	) );

	//Heading Six
	$wp_customize->add_section( 'clinic_pro_typography_section_headings_h6', array(
		'title'       => __( 'Heading Six', 'clinic-pro' ),
		'panel'		  => 'clinic_pro_panel_typography', 
		'priority'    => 10,
		'description' => __( 'Font for the Heading Six', 'clinic-pro' ),
	) );

}
add_action( 'customize_register', 'clinic_pro_kirki_section_typography' );
