<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Create sections using the WordPress Customizer API.
 */
function clinic_pro_kirki_section_color( $wp_customize ) {

	/**
	 * Add sections
	 */
	//Colors
	$wp_customize->add_section( 'clinic_pro_color_section_general', array(
		'title'			 => __( 'General', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage general colors', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_color_section_container', array(
		'title'			 => __( 'Layout Container', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage layout container colors', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_color_section_topbar', array(
		'title'			 => __( 'Top Bar', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage top bar colors', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_color_section_header', array(
		'title'			 => __( 'Header', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage header colors', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_color_section_pageheading', array(
		'title'			 => __( 'Page Heading', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage page heading colors - make them the same color if you want a solid bar rather than a gradient.<br/><br/>You will need to save and refresh to see these colors update.', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_color_section_footer', array(
		'title'			 => __( 'Footer', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_colors',
		'priority'		 => 10,
		'description'	 => __( 'Manage footer colors', 'clinic-pro' ),
	) );
}

add_action( 'customize_register', 'clinic_pro_kirki_section_color' );
