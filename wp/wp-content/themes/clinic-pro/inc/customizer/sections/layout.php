<?php

/**
 * Create sections using the WordPress Customizer API.
 */
function clinic_pro_kirki_section_layout( $wp_customize ) {

	/**
	 * Add sections
	 */
	//Layout
	$wp_customize->add_section( 'clinic_pro_layout_section_topbar', array(
		'title'			 => __( 'Top Bar Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the top bar', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_header_height', array(
		'title'			 => __( 'Header Height', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the header height', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_header_details', array(
		'title'			 => __( 'Header Details Visibility', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the header details', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_sticky', array(
		'title'			 => __( 'Sticky Header', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the sticky header', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_page_sidebar', array(
		'title'			 => __( 'Page Sidebar Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the Page Sidebar', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_blog_sidebar', array(
		'title'			 => __( 'Blog Sidebar Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the Blog Sidebar', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_blog_excerpt', array(
		'title'			 => __( 'Blog Excerpt Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage the Blog Excerpt', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_footer', array(
		'title'			 => __( 'Footer Layout Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage Footer Layout', 'clinic-pro' ),
	) );

	$wp_customize->add_section( 'clinic_pro_layout_section_footer_copyright', array(
		'title'			 => __( 'Footer Copyright Settings', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_layout',
		'priority'		 => 10,
		'description'	 => __( 'Manage Footer Copyright', 'clinic-pro' ),
	) );
}

add_action( 'customize_register', 'clinic_pro_kirki_section_layout' );
