<?php

/**
 * Create sections using the WordPress Customizer API.
 */
function clinic_pro_kirki_section_help( $wp_customize ) {

	/**
	 * Add sections
	 */
	//Help
	$wp_customize->add_section( 'clinic_pro_section_help_help', array(
		'title'			 => __( 'Clinic Pro Help', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_help',
		'priority'		 => 10,
		'description'	 => __( 'Get help', 'clinic-pro' ),
	) );
	$wp_customize->add_section( 'clinic_pro_section_help_support', array(
		'title'			 => __( 'Clinic Pro Support', 'clinic-pro' ),
		'panel'			 => 'clinic_pro_panel_help',
		'priority'		 => 10,
		'description'	 => __( 'Get premium customer support', 'clinic-pro' ),
	) );
}

add_action( 'customize_register', 'clinic_pro_kirki_section_help' );
