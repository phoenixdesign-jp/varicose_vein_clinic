<?php
/**
 * Getting started template
 */

$customizer_url = admin_url() . 'customize.php' ;
?>

<div id="changelog" class="ccfw-tab-pane">

	<div class="ccfw-tab-pane-center">
		
		<h1 class="ccfw-welcome-title"><?php esc_html_e( 'Clinic Pro Changelog','clinic-pro' ); ?></h1>

		<hr />

		<h2><?php esc_html_e( 'Version 1.1.1 - 25 July 2017.', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'Additional Theme Options added: Header Height adjustment, Show/Hide Back to Top, Header Details on Mobile, Show/Hide Sticky Header','clinic-pro' ); ?></p>

		<h2><?php esc_html_e( 'Version 1.1 - 17 July 2017.', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'General tweaks and improvements','clinic-pro' ); ?></p>
		<p><?php esc_html_e( 'Support for the Birch Scheduler plugin','clinic-pro' ); ?></p>
		<p><?php esc_html_e( 'Button style within menu','clinic-pro' ); ?></p>

		<h2><?php esc_html_e( 'Version 1.0.1 - 09 May 2017.', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'WooCommerce 3.0 support','clinic-pro' ); ?></p>
		<h2><?php esc_html_e( 'Version 1.0.0 - 16 March 2017.', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'Initial Release.','clinic-pro' ); ?></p>

	</div>

	<div class="ccfw-clear"></div>

</div>
