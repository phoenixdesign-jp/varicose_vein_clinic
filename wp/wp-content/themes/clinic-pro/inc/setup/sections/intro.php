<?php
/**
 * Getting started template
 *
 * @package CreateandCode
 * @subpackage clinic-pro
 */

$customizer_url 	= admin_url() . 'customize.php';
$theme_license_url 	= admin_url() . 'themes.php?page=clinic-pro-license';
?>

<div id="intro" class="ccfw-tab-pane active">

	<div class="primary-left">

	<div class="ccfw-tab-pane-center">

		<h1 class="ccfw-welcome-title"><?php esc_html_e( 'Welcome to Clinic Pro!', 'clinic-pro' ); ?></h1>

		<h2>
		<?php
		$theme_data = wp_get_theme();
		echo $theme_data->get( 'Description' );
		?>
			
		</h2>

		<hr />

		<h2 class="larger"><?php esc_html_e( 'Manage your Theme License', 'clinic-pro' ); ?></h2>

		<p><?php esc_html_e( 'First time here? If so, make sure you enter your theme license key to avail of automatic updates when they become available. If you have already entered your license key you can check its status from the Theme License page.', 'clinic-pro' ); ?></p>
		<p><a href="<?php echo esc_url( $theme_license_url ); ?>" class="button button-primary"><?php esc_html_e( 'Manage your Theme License', 'clinic-pro' ); ?></a></p>

		<hr />

		<h2 class="larger"><?php esc_html_e( 'Clinic Pro Theme Documentation', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'We provide lots of theme documentation articles including a detailed installation and setup guide on our website. A demo data file is also provided to get you up and running quickly.', 'clinic-pro' ); ?></p>
		<p><a target="_blank" href="<?php echo esc_url( 'https://createandcode.com/wordpress-themes/clinic-pro/help/' ); ?>" class="button button-primary"><?php esc_html_e( 'View Theme Documentation', 'clinic-pro' ); ?></a></p>

		<hr />

		<h2 class="larger"><?php esc_html_e( 'Theme Options', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'The Clinic Pro Theme Customizer enables you to customize many elements of the theme directly without any coding skills. This includes options such as uploading your logo, changing the primary color, and much more.', 'clinic-pro' ); ?></p>
		<ul>
		<li><?php esc_html_e( 'To access the Customizer, go to', 'clinic-pro' ); ?> <strong><?php esc_html_e( 'Appearance &#8594; Customize', 'clinic-pro' ); ?></strong> <?php esc_html_e( 'in the WordPress admin menu.', 'clinic-pro' ); ?></li>
		<li><?php esc_html_e( 'When you are finished making changes, click', 'clinic-pro' ); ?> <strong><?php esc_html_e( 'Save & Publish', 'clinic-pro' ); ?></strong> <?php esc_html_e( 'to save the settings. Check out your site to confirm your changes.', 'clinic-pro' ); ?></li>
		</ul>

		<p><a href="<?php echo esc_url( $customizer_url ); ?>" class="button button-primary"><?php esc_html_e( 'Launch the Customizer', 'clinic-pro' ); ?></a></p>

	</div>

	</div><!--/primary-left -->

	<div class="primary-right">

	<div class="ccfw-screenshot">
		<img src="<?php echo get_template_directory_uri() . '/screenshot.png'; ?>" alt="" />
	</div>

	<div class="ccfw-review">
		<h2><?php esc_html_e( 'Submit a Support Ticket', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'If you have any questions on Clinic Pro that are not answered via the documentation, just submit a ticket to our helpdesk. We will be very happy to help!', 'clinic-pro' ); ?></p>

		<a class="button-primary" target="_blank" href="https://createandcode.freshdesk.com/helpdesk/tickets/new"><?php esc_html_e( 'Submit a Ticket', 'clinic-pro' ); ?></a>
		<i class="dashicons dashicons-admin-comments"></i>
	</div>

	<div class="ccfw-review">
		<h2><?php esc_html_e( 'Visit our Blog', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'For reviews, guides, tips on speed, security and lots more check out our blog over on Create and Code. We also have a coupons section with special offers on a number of services and plugins.', 'clinic-pro' ); ?></p>

		<a class="button-primary" target="_blank" href="https://createandcode.com/blog"><?php esc_html_e( 'Visit our Blog', 'clinic-pro' ); ?></a>
		<i class="dashicons dashicons-images-alt2"></i>
	</div>

	<div class="ccfw-review">
		<h2><?php esc_html_e( 'Join our Facebook group!', 'clinic-pro' ); ?></h2>
		<p><?php esc_html_e( 'Get tips, tricks and support from the Create & Code Community. We would also love to see what you have created with our WordPress themes. Share your work with the community!', 'clinic-pro' ); ?></p>

		<a class="button-primary" target="_blank" href="https://www.facebook.com/groups/1974135042612016/"><?php esc_html_e( 'Join now', 'clinic-pro' ); ?></a>
		<i class="dashicons dashicons-groups"></i>
	</div>

	</div><!--/primary-right -->

	<div class="ccfw-clear"></div>

</div>
