<?php

/**
 * Theme onboarding and help.
 *
 * @package clinic-pro
 */
class clinic_pro_Help {

	/**
	 * Constructor
	 * Sets up the welcome screen
	 */
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'clinic_pro_help_register_menu' ) );
		add_action( 'load-themes.php', array( $this, 'clinic_pro_help_activation_admin_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'clinic_pro_help_assets' ) );

		add_action( 'clinic_pro_help', array( $this, 'clinic_pro_help_intro' ), 10 );
	}

// end constructor

	/**
	 * Redirect to Onboarding page upon theme switch/activation
	 */
	public function clinic_pro_help_activation_admin_init() {
		global $pagenow;

		if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) { // input var okay
			add_action( 'admin_notices', array( $this, 'clinic_pro_welcome_admin_notice' ), 99 );
		}
	}

	/**
	 * Display an admin notice linking to the welcome screen
	 * @since 1.0.3
	 */
	public function clinic_pro_welcome_admin_notice() {
		?>
			<div class="updated notice is-dismissible">
				<p><?php echo sprintf( esc_html__( 'Thanks for choosing Clinic Pro! You can read hints and tips on how get the most out of your new theme in the %sHelp Centre%s.', 'clinic-pro' ), '<a href="' . esc_url( admin_url( 'themes.php?page=ccfw-help' ) ) . '">', '</a>' ); ?></p>
				<p><a href="<?php echo esc_url( admin_url( 'themes.php?page=ccfw-help' ) ); ?>" class="button" style="text-decoration: none;"><?php _e( 'Get started with Clinic', 'clinic-pro' ); ?></a></p>
			</div>
		<?php
	}

	// Help assets
	public function clinic_pro_help_assets( $hook_suffix ) {
		global $clinic_pro_version;

		if ( 'appearance_page_ccfw-help' == $hook_suffix ) {
			wp_enqueue_style( 'ccfw-help', get_template_directory_uri() . '/inc/setup/help.css', $clinic_pro_version );
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_script( 'thickbox' );
			wp_enqueue_script( 'ccfw-help', get_template_directory_uri() . '/inc/setup/help.js', array( 'jquery' ), '1.0.0', true );
		}
	}

	// Quick Start menu 
	public function clinic_pro_help_register_menu() {
		add_theme_page( 
			__( 'Clinic Pro Help', 'clinic-pro' ),
			__( 'Clinic Pro Help', 'clinic-pro' ),
			'activate_plugins', 
			'ccfw-help', 
			array( $this, 'clinic_pro_help_screen' ) );
	}

	/**
	 * The welcome screen
	 *
	 * @since 1.0.0
	 *
	 */
	public function clinic_pro_help_screen() {
	?>
		<div class="ccfw-help container">

			<h1 class="ccfw-help-title"><?php _e( 'Clinic Pro Help Centre', 'clinic-pro' ); ?></h1>
			<h2 class="ccfw-help-desc"><?php _e( 'Everything you need to know to get the most out of Clinic Pro.', 'clinic-pro' ); ?></h2>
			<ul class="ccfw-nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#intro" aria-controls="getting_started" role="tab" data-toggle="tab"><?php esc_html_e( 'Getting Started', 'clinic-pro' ); ?></a></li>
				
			</ul>

			<div class="ccfw-tab-content">
				<?php
				/**
				 * @hooked clinic_pro_welcome_intro - 10
				 */
				do_action( 'clinic_pro_help' );
				?>


			</div>
		</div>
		<?php
	}

	public function clinic_pro_help_intro() {
		require_once( get_template_directory() . '/inc/setup/sections/intro.php' );
	}

}

$GLOBALS['clinic_pro_help'] = new clinic_pro_Help();
