<?php
/**
 * General functions used to integrate with WooCommerce.
 *
 * @package clinic-pro
 */

/**
 * Before Content
 * Wraps all WooCommerce content in wrappers which match the theme markup
 * @since   1.0.0
 * @return  void
 */

if ( ! function_exists( 'clinic_pro_header_before_content' ) ) {
	function clinic_pro_header_before_content() {
		?>

		<div class="ccfw-content">
		<header class="entry-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12">
	                    <div class="ccfw-page-title">
	                    <h1><?php woocommerce_page_title(); ?></h1>
	                    </div>

	                    <?php
	                        if ( function_exists('yoast_breadcrumb') ) {
	                        echo ('<div class="ccfw-breadcrumbs">');
	                        yoast_breadcrumb('<p>','</p>');
	                        echo ('</div>');
	                        }
	                    ?>

	                </div>
				</div>
			</div>
		</header><!-- .entry-header -->
		

	    	<?php
	}
}

if ( ! function_exists( 'clinic_pro_before_content' ) ) {
	function clinic_pro_before_content() {
		?>
		<div class="container ccfw-shop-main">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-md-push-3 col-lg-push-3 right-content">

	    	<?php
	}
}


/**
 * After Content
 * Closes the wrapping divs
 * @since   1.0.0
 * @return  void
 */
if ( ! function_exists( 'clinic_pro_after_content' ) ) {
	function clinic_pro_after_content() {
		?>
				</div>
				<div class="col-lg-3 col-md-3 col-md-pull-9 col-lg-pull-9 sidebar">
            		<div id="secondary">
						<?php dynamic_sidebar( 'sidebar-shop' ); ?>
					</div>	
            	</div>

			</div>
		</div>
	</div>
	<?php }
}

/**
 * Removes page titles
 * @since   1.0.0
 * @return  void
 */
function clinic_pro_override_page_title() {
return false;
}

add_filter('woocommerce_show_page_title', 'clinic_pro_override_page_title');

// Change number or products per row to 3
add_filter('loop_shop_columns', 'clinic_pro_loop_columns');
if (!function_exists('clinic_pro_loop_columns')) {
	function clinic_pro_loop_columns() {
		return 3; // 3 products per row
	}
}

// Display 9 products per page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20 );

// Change number of related products per row to 3
add_filter( 'woocommerce_output_related_products_args', 'clinic_pro_related_products_count' );
 
function clinic_pro_related_products_count( $args ) {
     $args['posts_per_page'] = 3;
     $args['columns'] = 3;
 
     return $args;
}

// Change number of upsell products per row to 3
add_filter( 'woocommerce_output_upsells_args', 'clinic_pro_upsell_products_count' );
 
function clinic_pro_upsell_products_count( $args ) {
     $args['posts_per_page'] = 3;
     $args['columns'] = 3;
 
     return $args;
}

// Remove Jetpack's Related Posts for WooCommerce
function clinic_pro_jetpack_remove_rp() {
    if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
        $jprp = Jetpack_RelatedPosts::init();
        $callback = array( $jprp, 'filter_add_target_to_dom' );
        remove_filter( 'the_content', $callback, 40 );
    }
}
add_filter( 'wp', 'clinic_pro_jetpack_remove_rp', 20 );

?>