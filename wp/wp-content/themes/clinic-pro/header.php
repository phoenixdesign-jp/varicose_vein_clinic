<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package clinic-pro
 */
$clinic_pro_layout_topbar	 = '';
$clinic_pro_layout_topbar	 = clinic_pro_get_option( 'clinic_pro_layout_topbar' );

$clinic_pro_sticky_header	 						= '';
$clinic_pro_sticky_header	 						= clinic_pro_get_option( 'clinic_pro_sticky_header' );

$clinic_pro_layout_header_details_visibility		=	'';
$clinic_pro_layout_header_details_visibility	 	= clinic_pro_get_option( 'clinic_pro_layout_header_details_visibility' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131359039-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131359039-4');
  gtag('config', 'UA-75897376-1');
</script>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>	
<script>
  (function(d) {
    var config = {
      kitId: 'swq4rmn',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8ZQ24M');</script>
<!-- End Google Tag Manager -->
	</head>
	<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8ZQ24M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->		
		<div id="wrapper">	
			<header id="ccfw-header-wrap">
				<?php if ( "show" == $clinic_pro_layout_topbar ) { ?>
					<div class="ccfw-header-topbar">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6 top-bar-left">
									<?php do_action( 'clinic_pro_topbar' ); ?>
									<?php if ( is_active_sidebar( 'top-bar-left' ) ) : ?>
										<?php dynamic_sidebar( 'top-bar-left' ); ?>   
									<?php endif; ?>
								</div>
								<div class="col-sm-6 col-md-6 col-lg-6 top-bar-right">
									<?php do_action( 'clinic_pro_topbar' ); ?>
									<?php if ( is_active_sidebar( 'top-bar-right' ) ) : ?>
										<?php dynamic_sidebar( 'top-bar-right' ); ?>   
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				<div class="ccfw-header-main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12">
								<?php do_action( 'clinic_pro_header' ); ?>
								<div class="ccfw-header-branding">
								<?php if ( is_active_sidebar( 'header-right' ) ) : ?>
										<div class="ccfw-header-details-right">
											<?php dynamic_sidebar( 'header-right' ); ?>
										</div>
									<?php endif; ?>
									<?php if ( is_active_sidebar( 'header-left' ) ) : ?>

										<?php if ( "show" == $clinic_pro_layout_header_details_visibility ) { ?>
										<div class="ccfw-header-details">
											
										<?php } elseif ( 'hide' === $clinic_pro_layout_header_details_visibility ) { ?>
										<div class="ccfw-header-details hide-on-mobile">
										<?php } ?>
										<?php dynamic_sidebar( 'header-left' ); ?>
										</div>
									<?php endif; ?> 
									
								</div>               
							</div>
						</div>
					</div>
				</div>
				<div class="ccfw-header-nav">
					<?php do_action( 'clinic_pro_header_navigation' ); ?>
				</div>
				<div class="ccfw-header-before-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12">
								<?php do_action( 'clinic_pro_header_before_content' ); ?>
							</div>
						</div>
					</div>
				</div>
			</header>

			<?php if ( "show" == $clinic_pro_sticky_header ) { ?>
			<div class="sticky-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 display-table">
							<?php do_action( 'clinic_pro_header' ); ?>
							<?php do_action( 'clinic_pro_header_navigation' ); ?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>

			<div id="ccfw-page-wrap" class="hfeed site">