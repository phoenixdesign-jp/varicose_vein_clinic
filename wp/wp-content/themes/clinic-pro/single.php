<?php
/**
 * The template for displaying all single posts.
 *
 * @package clinic-pro
 */
$clinic_pro_layout_blog_listing_sidebar	 = '';
$clinic_pro_layout_blog_listing_sidebar	 = clinic_pro_get_option( 'clinic_pro_layout_blog_listing_sidebar' );

get_header();
?>

<div class="ccfw-content">

	<header class="entry-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<?php if ( 'post' == get_post_type() ) : ?>
						<div class="ccfw-entry-meta">
							<?php clinic_pro_posted_on(); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header>

	<?php
		if ( function_exists('yoast_breadcrumb') ) {
		echo ('<div class="ccfw-breadcrumbs"><div class="container"><div class="row"><div class="col-lg-12 col-md-12">');
		yoast_breadcrumb('<p>','</p>');
		echo ('</div></div></div></div>');
		}
	?>

	<div class="container">
		<div class="row">
			<?php if ( $clinic_pro_layout_blog_listing_sidebar == 'left' ) { ?>
				<div class="col-lg-9 col-md-9 col-md-push-3 col-lg-push-3 right-content">
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
				</div>
				<div class="col-lg-3 col-md-3 col-md-pull-9 col-lg-pull-9">
					<?php get_sidebar(); ?>
				</div>
			<?php } else if ( $clinic_pro_layout_blog_listing_sidebar == 'none' ) { ?>
				<div class="col-lg-12 col-md-12">
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
				</div>
			<?php } else if ( ( $clinic_pro_layout_blog_listing_sidebar == 'right' ) || ( $clinic_pro_single_blog_sidebar_position == '' ) ) { ?>
				<div class="col-lg-9 col-md-9 left-content">
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
					<!-- バナー追加 -->
					<a href="https://medical.apokul.jp/web/47/reservations/add?_ga=2.148601202.453510702.1607900195-630200188.1604551223" target="_blank" style="display: block; margin-bottom: 50px;">
						<img src="https://shizuoka-varix.com/wp/wp-content/uploads/2020/12/foot_banner.png" alt="静岡静脈瘤クリニック ネット予約(初診)はこちらから">
					</a>
					<!-- / バナー追加 -->
				</div>
				<div class="col-lg-3 col-md-3">
					<?php get_sidebar(); ?>
				</div>
			<?php } ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>
