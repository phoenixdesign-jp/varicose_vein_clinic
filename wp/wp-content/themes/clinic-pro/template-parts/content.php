<?php

/**
 * @package clinic-pro
 */
$clinic_pro_layout_blog_listing_featured_img = '';
$clinic_pro_layout_blog_listing_featured_img = clinic_pro_get_option( 'clinic_pro_layout_blog_listing_featured_img' );
?>
<?php

if ( ( has_post_thumbnail() ) && ( 'hide' !== $clinic_pro_layout_blog_listing_featured_img ) ) {
	get_template_part( 'template-parts/blog', 'loop-item-with-thumb' );
} else {
	get_template_part( 'template-parts/blog', 'loop-item' );
}
?>