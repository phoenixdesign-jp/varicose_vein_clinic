<?php
/**
 * The template used for displaying full width page content in page.php
 *
 * @package clinic-pro
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php //clinic_pro_get_page_title(); ?>
    <div class="entry-content">
		<?php the_content(); ?>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'clinic-pro' ),
			'after'	 => '</div>',
		) );
		?>
    </div><!-- .entry-content -->
</article><!-- #post-## -->
