<?php
$clinic_pro_layout_blog_listing_featured_img = '';
$clinic_pro_layout_blog_listing_featured_img = clinic_pro_get_option( 'clinic_pro_layout_blog_listing_featured_img' );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'ccfw-blog-loop-item' ); ?>>
	<?php {
		echo clinic_pro_blog_image();
		echo clinic_pro_blog_loop_item();
	}
	?>
</article><!-- #post-## -->