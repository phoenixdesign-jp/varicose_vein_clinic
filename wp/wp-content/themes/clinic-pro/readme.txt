/*========= Clinic =========*/

Theme Name: Clinic Pro
Theme URI: https://createandcode.com/wordpress-themes/clinic-pro/
Version: 1.1.3
Tested up to: WP 4.9+

Author: Create and Code
Author URI: https://createandcode.com/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl.html
-------------------------------------------------------
Clinic Pro theme, Copyright 2018 createandcode.com
Clinic Pro WordPress theme is distributed under the terms of the GNU GPL
Clinic Pro is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
-------------------------------------------------------

/*========= Credits =========*/
Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 license

Clinic Pro uses:
* FontAwesome (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL)
* IonIcons (http://ionicons.com) licensed under the MIT license (https://opensource.org/licenses/MIT)
* Bootstrap (http://getbootstrap.com/) licensed under MIT license (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* Unless otherwise specified, all images are licensed under Creative Commons and referenced within images.txt

/*========= Description =========*/

Clinic Pro is a WordPress theme designed especially for medical-based businesses such as doctors, dentists, hospitals and medical centres but it can be used for any kind of professional firm.

For questions, comments or bug reports, visit http://www.createandcode.com/support/

/*========= Installation =========*/

You can download the file, unzip it and move the unzipped contents to the "wp-content/themes" folder of your WordPress installation. You will then be able to activate the theme.

Afterwards you can continue theme setup and customization via WordPress Dashboard -> Appearance -> Customize. For detailed theme documentation, please visit http://www.createandcode.com/support/

/*========= Theme Features =========*/

* Bootstrap 3 integration
* Responsive design
* Unlimited color variations
* SEO friendly
* WordPress Theme Customizer support
* Internationalized & localization
* Drop-down Menu
* Cross-browser compatibility
* Threaded Comments
* Gravatar ready
* Font Awesome icons

/*========= Documentation =========*/

Theme documentation is available on http://www.createandcode.com/support/

/*========= Changelog =========*/

= 1.1.3 - 19.06.2018 =
Improved setup and help documentation

= 1.1.2 - 19.12.2017 =
Sticky menu z-index fix

= 1.1.1 - 25.07.2017 =
Additional Theme Options added: Header Height adjustment, Show/Hide Back to Top, Header Details on Mobile, Show/Hide Sticky Header

= 1.1.0 - 19.07.2017 =
General tweaks and improvements
Support for the Birch Scheduler plugin
Button style within menu

= 1.0.1 - 09.05.2017 =
WooCommerce 3.0 support

= 1.0.0 - 14.01.2017 =
Initial release
