<?php
/**
 * Template Name: Full width page
 * @package clinic-pro
 */

$clinic_pro_featured_image_class         = '';
$clinic_pro_header_image_style           = '';
$clinic_pro_header_image = '';

get_header();
?>
<div class="ccfw-content">

    <header class="entry-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="ccfw-page-title">
                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                    </div>

                    <?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        echo ('<div class="ccfw-breadcrumbs">');
                        yoast_breadcrumb('<p>','</p>');
                        echo ('</div>');
                        }
                    ?>

                </div>
            </div>
        </div>
    </header><!-- .entry-header -->

    <div class="container content-container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'template-parts/content', 'page' ); ?>

							<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>

						<?php endwhile; // end of the loop.  ?>

                    </main>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

