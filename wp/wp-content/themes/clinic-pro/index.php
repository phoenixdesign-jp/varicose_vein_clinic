<?php
/**
 * @package clinic-pro
 */
$clinic_pro_layout_blog_listing_sidebar	 = '';
$clinic_pro_layout_blog_listing_sidebar	 = clinic_pro_get_option( 'clinic_pro_layout_blog_listing_sidebar' );
$clinic_pro_featured_image_class		 = '';
$clinic_pro_header_image_style			 = '';

get_header();
?>

<div class="ccfw-content">

	<header class="entry-header">

		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php
					if ( is_home() && get_option( 'page_for_posts' ) ) {
						$clinic_pro_blog_page_title = get_option( 'page_for_posts' );
						echo '<div class="ccfw-page-title"><h1>' . get_page( $clinic_pro_blog_page_title )->post_title . '</h1></div>';
					} else { ?>
						<div class="ccfw-page-title"><h1><?php echo _e( 'Latest Posts', 'clinic-pro' ); ?></h1></div>
					<?php }
					?>

					<?php
                        if ( function_exists('yoast_breadcrumb') ) {
                        echo ('<div class="ccfw-breadcrumbs">');
                        yoast_breadcrumb('<p>','</p>');
                        echo ('</div>');
                        }
                    ?>

				</div>
			</div>
		</div>
	</header><!-- .entry-header -->

	<?php
		if ( function_exists('yoast_breadcrumb') ) {
		echo ('<div class="ccfw-breadcrumbs"><div class="container"><div class="row"><div class="col-lg-12 col-md-12">');
		yoast_breadcrumb('<p>','</p>');
		echo ('</div></div></div></div>');
		}
	?>

	<div class="container">
		<div class="row">
			<?php if ( ( 'left' == $clinic_pro_layout_blog_listing_sidebar ) || ( '' == $clinic_pro_layout_blog_listing_sidebar ) ) { ?>
				<div class="col-lg-9 col-md-9 col-md-push-3 col-lg-push-3 right-content">
					<?php get_template_part( 'template-parts/blog', 'list' ); ?>
				</div>
				<div class="col-lg-3 col-md-3 col-md-pull-9 col-lg-pull-9">
					<?php get_sidebar(); ?>
				</div>
			<?php } else if ( 'right' == $clinic_pro_layout_blog_listing_sidebar ) { ?>
				<div class="col-lg-9 col-md-9 left-content">
					<?php get_template_part( 'template-parts/blog', 'list' ); ?>
				</div>
				<div class="col-lg-3 col-md-3">
					<?php get_sidebar(); ?>
				</div>
			<?php } else if ( 'none' == $clinic_pro_layout_blog_listing_sidebar ) { ?>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<?php get_template_part( 'template-parts/blog', 'list' ); ?>
				</div>
			<?php }
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
