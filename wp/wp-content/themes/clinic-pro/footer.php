<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package clinic-pro
 */
$clinic_pro_layout_first_footer					 = '';
$clinic_pro_layout_second_footer				 = '';
$clinic_pro_color_footer_copyright_background	 = '';
$clinic_pro_footer_copyright_text				 = clinic_pro_get_option( 'clinic_pro_footer_copyright_text' );
$clinic_pro_layout_first_footer					 = clinic_pro_get_option( 'clinic_pro_layout_first_footer' );

$clinic_pro_backtotop	 						 = '';
$clinic_pro_backtotop	 						 = clinic_pro_get_option( 'clinic_pro_backtotop' );
?>
<footer class="ccfw-footer-container"> 
	<?php if ( 'show' == $clinic_pro_layout_first_footer ) { ?>
		<div class="ccfw-first-footer-wrapper">
			<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'first-footer-1' ) ) : ?>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<?php dynamic_sidebar( 'first-footer-1' ); ?>   
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'first-footer-2' ) ) : ?>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<?php dynamic_sidebar( 'first-footer-2' ); ?>   
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( 'first-footer-3' ) ) : ?>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<?php dynamic_sidebar( 'first-footer-3' ); ?>   
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php } ?>
	<!-- /first footer -->

	<div class="ccfw-last-footer">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="ccfw-footer-msg">
					<div class="site-info">
	    				<?php echo clinic_pro_safe_html( $clinic_pro_footer_copyright_text ); ?>
					</div><!-- /site-info -->
					<?php if ( 'show' == $clinic_pro_backtotop ) { ?>
						<a href="#" id="ccfw-back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
					<?php } ?>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

</footer>
</div><!-- /#ccfw-page-wrap -->
</div><!-- /#wrapper -->
<?php wp_footer(); ?>
</body>
</html>
