<?php
/**
 * Clinic functions and definitions
 *
 * @package clinic-pro
 */

/**
 * 親テーマの読み込み
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function clinic_pro_widgets_init2() {
	register_sidebar( array(
		'name'			 => __( 'Column Sidebar', 'clinic-pro' ),
		'id'			 => 'sidebar-column',
		'description'    => __( 'column sidebar.', 'clinic-pro' ),
		'before_widget'	 => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</aside>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );//カスタム投稿「コラム」のサイドバー
}

add_action( 'widgets_init', 'clinic_pro_widgets_init2',11 );


/**
 * カスタム投稿「コラム」の月別アーカイブ出力
 */
global $my_archives_post_type;
add_filter( 'getarchives_where', 'my_getarchives_where', 10, 2 );
function my_getarchives_where( $where, $r ) {
  global $my_archives_post_type;
  if ( isset($r['post_type']) ) {
    $my_archives_post_type = $r['post_type'];
    $where = str_replace( '\'post\'', '\'' . $r['post_type'] . '\'', $where );
  } else {
    $my_archives_post_type = '';
  }
  return $where;
}
add_filter( 'get_archives_link', 'my_get_archives_link' );
function my_get_archives_link( $link_html ) {
  global $my_archives_post_type;
  if ( '' != $my_archives_post_type )
    $add_link .= '?post_type=' . $my_archives_post_type;
	$link_html = preg_replace("/href=\'(.+)\'\s/","href='$1".$add_link." '",$link_html);

  return $link_html;
}


/**
 * カスタム投稿「コラム」の記事一覧ウィジェット
 */

class NewEntryWidgetItem extends WP_Widget {
	function NewEntryWidgetItem() {
		parent::__construct(false, $name = 'コラム一覧');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数を取得
	   $entry_count = apply_filters( 'widget_entry_count', $instance['entry_count'] );
	   //日付を表示するを取得
	   $date_show = apply_filters( 'widget_entry_count', $instance['date_show'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_count;
	   global $g_date_show;
	   $g_entry_title = '最近の投稿';//表示数が設定されていない時は5にする
	   if($title_new){//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
	   $g_entry_count = 5;//表示数が設定されていない時は5にする
	   if ($entry_count) {//表示数が設定されているときは表示数をグローバル変数に代入
		 $g_entry_count = $entry_count;
	   }
	   $g_date_show = $date_show;
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 global $col_args;
			 $col_args = array(
				'post_type' => 'column',
				'posts_per_page' => $g_entry_count
			  );
			echo $args['before_widget'];
			echo $args['before_title'] . $g_entry_title . $args['after_title'];//見出し
			get_template_part('template-parts/column','list');
			echo $args['after_widget'];
			?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	$instance['entry_count'] = strip_tags($new_instance['entry_count']);
	$instance['date_show'] = strip_tags($new_instance['date_show']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   $entry_count = esc_attr($instance['entry_count']);
	   $date_show = esc_attr($instance['date_show']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラム一覧のタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('entry_count'); ?>">
		 <?php _e('表示数（半角数字）'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('entry_count'); ?>" name="<?php echo $this->get_field_name('entry_count'); ?>" type="number" value="<?php echo $entry_count; ?>" />
	   </p>
	   <?php //日付を表示するフォーム ?>
	   <p>
		 <input class="widefat" id="<?php echo $this->get_field_id('date_show'); ?>" name="<?php echo $this->get_field_name('date_show'); ?>" type="checkbox" value="1" <?php checked( $instance['date_show'], 1 ); ?>/>
		 <label for="<?php echo $this->get_field_id('date_show'); ?>">
		 <?php _e('投稿日を表示しますか ?'); ?>
		 </label>
	   </p>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetItem");'));


/**
 * カスタム投稿「コラム」のアーカイブウィジェット
 */

class NewEntryWidgetarchive extends WP_Widget {
	function NewEntryWidgetarchive() {
		parent::__construct(false, $name = 'コラムアーカイブ');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_title;
	   $g_entry_title = 'アーカイブ';//表示数が設定されていない時は5にする
	   if ($title_new) {//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 get_template_part('template-parts/column','archive'); ?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラムアーカイブのタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetarchive");'));


/**
 * カスタム投稿「コラム」のカテゴリウィジェット
 */

class NewEntryWidgetcategory extends WP_Widget {
	function NewEntryWidgetcategory() {
		parent::__construct(false, $name = 'コラムカテゴリ');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_title;
	   $g_entry_title = 'カテゴリー';//表示数が設定されていない時は5にする
	   if ($title_new) {//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 get_template_part('template-parts/column','category'); ?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラムアーカイブのタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetcategory");'));
