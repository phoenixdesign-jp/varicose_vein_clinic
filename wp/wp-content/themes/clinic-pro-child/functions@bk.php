<?php
/**
 * Clinic functions and definitions
 *
 * @package clinic-pro
 */

/**
 * 親テーマの読み込み
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

/**
 * Assign the theme version to a var
 */
$theme			 = wp_get_theme( 'clinic-pro' );
$clinic_pro_version	 = $theme['Version'];

/**
 * Global Paths
 */
define( 'CC_CORE', get_template_directory() . '/inc/core' );

/**
 * Load theme updater functions.
 * Action is used so that child themes can easily disable.
 */
function clinic_pro_theme_updater() {
	require( CC_CORE . '/updater/theme-updater.php' );
}
add_action( 'after_setup_theme', 'clinic_pro_theme_updater' );

/**
 * Load styles
 */
function clinic_pro_load_styles() {
	global $clinic_pro_live_preview;
	wp_enqueue_style( 'ccfw-style', get_stylesheet_uri() );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome/font-awesome.min.css' );
	wp_enqueue_style( 'ccfw-ionicons', get_template_directory_uri() . '/css/ionicons.css' );
	wp_enqueue_style( 'ccfw-elementor', get_template_directory_uri() . '/css/elementor.css' );
	wp_enqueue_style( 'ccfw-animate', get_template_directory_uri() . '/css/animate.css' );
	wp_enqueue_style( 'ccfw-extras', get_template_directory_uri() . '/css/extras.css' );

}

add_action( 'wp_enqueue_scripts', 'clinic_pro_load_styles' );

/**
 * TGM Plugin Activation
 */
require_once( CC_CORE . '/functions/class-tgm-plugin-activation.php' );
add_action( 'tgmpa_register', 'clinic_pro_register_required_plugins' );

/**
 * Recommended plugins
 *
 * @package clinic-pro
 */
function clinic_pro_register_required_plugins() {
	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		array(
			'name'		 => 'One Click Demo Import', // The plugin name
			'slug'		 => 'one-click-demo-import', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required
		),
		array(
			'name'		 => 'Advanced Sidebar Menu', // The plugin name
			'slug'		 => 'advanced-sidebar-menu', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required
		),
		array(
			'name'		 => 'Kirki', // The plugin name
			'slug'		 => 'kirki', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required.
		),
		array(
			'name'		 => 'Contact Form 7', // The plugin name
			'slug'		 => 'contact-form-7', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required.
		),
		array(
			'name'		 => 'Elementor', // The plugin name
			'slug'		 => 'elementor', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required.
		),
		array(
			'name'		 => 'Crelly Slider', // The plugin name
			'slug'		 => 'crelly-slider', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required.
		),
		array(
			'name'		 => 'BirchPress Scheduler', // The plugin name
			'slug'		 => 'birchschedule', // The plugin slug (typically the folder name)
			'required'	 => false, // If false, the plugin is only 'recommended' instead of required.
		),
	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'		 => 'clinic-pro', // Text domain - likely want to be the same as your theme.
		'default_path'	 => '', // Default absolute path to pre-packaged plugins
		'parent_slug'	 => 'themes.php', // Default parent menu slug
		'menu'			 => 'tgmpa-install-plugins', // Menu slug
		'has_notices'	 => true, // Show admin notices or not
		'is_automatic'	 => false, // Automatically activate plugins after installation or not
		'message'		 => '', // Message to output right before the plugins table.
		'strings'		 => array(
			'page_title'						 => esc_html__( 'Install Required Plugins', 'clinic-pro' ),
			'menu_title'						 => esc_html__( 'Install Plugins', 'clinic-pro' ),
			'installing'						 => esc_html__( 'Installing Plugin: %s', 'clinic-pro' ), // %1$s = plugin name
			'oops'								 => esc_html__( 'Something went wrong with the plugin API.', 'clinic-pro' ),
			'notice_can_install_required'		 => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'	 => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_cannot_install'				 => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_can_activate_required'		 => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'	 => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_cannot_activate'			 => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_ask_to_update'				 => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'clinic-pro' ), // %1$s = plugin name(s)
			'notice_cannot_update'				 => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'clinic-pro' ), // %1$s = plugin name(s)
			'install_link'						 => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'clinic-pro' ),
			'activate_link'						 => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'clinic-pro' ),
			'return'							 => esc_html__( 'Return to Required Plugins Installer', 'clinic-pro' ),
			'plugin_activated'					 => esc_html__( 'Plugin activated successfully.', 'clinic-pro' ),
			'complete'							 => esc_html__( 'All plugins installed and activated successfully. %s', 'clinic-pro' ), // %1$s = dashboard link
			'nag_type'							 => 'updated', // Determines admin notice type - can only be 'updated' or 'error'.
		),
	);
	tgmpa( $plugins, $config );
}

// One Click Importer Demo Data.
function ocdi_import_files() {
	return array(
	array(
	  'import_file_name'           => 'Clinic Pro Demo Data',
	  'import_file_url'            => 'http://assets.createandcode.com/clinic-pro/clinic-demodata.xml',
	  'import_widget_file_url'     => 'http://assets.createandcode.com/clinic-pro/clinic-widgets.json',
	  'import_preview_image_url'   => 'http://themedemo.createandcode.com/clinic-pro/wp-content/themes/clinic-pro/screenshot.png',
	  'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'clinic-pro' ),
	),

	);
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );

function ocdi_after_import_setup() {
	// Assign menus to their locations.
	$main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
			'primary' => $main_menu->term_id,
		)
	);

	// Assign front page and posts page (blog page).
	$front_page_id = get_page_by_title( 'Home' );
	$blog_page_id  = get_page_by_title( 'News' );

	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );

if ( ! function_exists( 'clinic_pro_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function clinic_pro_setup() {
		/* Help */
		if ( is_admin() ) {
			// Welcome page.
			require_once( get_template_directory() . '/inc/setup/help.php' );
		}

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on clinic, use a find and replace
		 * to change 'clinic-pro' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'clinic-pro', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Custom Thumbnails.
		if ( function_exists( 'add_theme_support' ) ) {
			add_theme_support( 'post-thumbnails' );
			add_image_size( 'ccfw-blog-featured-top-withsidebar', 760, 380, true );
			add_image_size( 'ccfw-blog-featured-top-nosidebar', 1070, 535, true );
			add_image_size( 'ccfw-blog-featured-top', 760, 380, true );
		}

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'clinic-pro' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Adds support for selective refresh of widgets
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'clinic_pro_custom_background_args', array(
			'default-color'	 => 'ffffff',
			'default-image'	 => '',
		) ) );

		add_editor_style( 'css/editor-style.css' );

		// Add theme support for Custom Logo.
		add_theme_support( 'custom-logo', array(
			'width'       => 400,
			'height'      => 107,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		require_once( trailingslashit( get_template_directory() ) . 'trt-customize-pro/example-1/class-customize.php' );
	}
endif; // clinic_pro_setup.
add_action( 'after_setup_theme', 'clinic_pro_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function clinic_pro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'clinic_pro_content_width', 760 );
}

add_action( 'after_setup_theme', 'clinic_pro_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function clinic_pro_widgets_init() {
	register_sidebar( array(
		'name'			 => esc_html__( 'Sidebar', 'clinic-pro' ),
		'id'			 => 'sidebar-1',
		'description'   => __( 'Main sidebar.', 'clinic-pro' ),
		'before_widget'	 => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</aside>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Top Bar Left', 'clinic-pro' ),
		'id'			 => 'top-bar-left',
		'description'   => __( 'This widget area appears in the top left of the screen in the top bar.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Top Bar Right', 'clinic-pro' ),
		'id'			 => 'top-bar-right',
		'description'   => __( 'This widget area appears in the top right of the screen in the top bar.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Header Left', 'clinic-pro' ),
		'id'			 => 'header-left',
		'description'   => __( 'This widget area appears in the top left of the header.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Header Right', 'clinic-pro' ),
		'id'			 => 'header-right',
		'description'   => __( 'This widget area appears in the top right of the header.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Pages Sidebar', 'clinic-pro' ),
		'id'			 => 'sidebar-pages',
		'description'   => __( 'This widget appears on pages only.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Shop Sidebar', 'clinic-pro' ),
		'id'			 => 'sidebar-shop',
		'description'    => __( 'This appears if you have WooCommerce enabled.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Footer Column 1', 'clinic-pro' ),
		'id'			 => 'first-footer-1',
		'description'   => __( 'The first footer widget area.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Footer  Column 2', 'clinic-pro' ),
		'id'			 => 'first-footer-2',
		'description'   => __( 'The second footer widget area.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Footer  Column 3', 'clinic-pro' ),
		'id'			 => 'first-footer-3',
		'description'   => __( 'The third footer widget area.', 'clinic-pro' ),
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );
	register_sidebar( array(
		'name'			 => __( 'Column Sidebar', 'clinic-pro' ),
		'id'			 => 'sidebar-column',
		'description'    => __( 'column sidebar.', 'clinic-pro' ),
		'before_widget'	 => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</aside>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );//カスタム投稿「コラム」のサイドバー
}

add_action( 'widgets_init', 'clinic_pro_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function clinic_pro_scripts() {
	wp_enqueue_style( 'ccfw-style', get_stylesheet_uri() );
	wp_enqueue_script( 'ccfw-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20161205', true );
	wp_enqueue_script( 'ccfw-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '20161205', true );
	wp_enqueue_script( 'ccfw-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'clinic_pro_scripts' );


/**
 * Custom Logo Markup
 */
add_filter( 'get_custom_logo', 'clinic_pro_custom_logo' );
function clinic_pro_custom_logo() {
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url"><span class="helper"></span>%2$s</a>',
		esc_url( home_url( '/' ) ),
		wp_get_attachment_image( $custom_logo_id, 'full', false, array(
				'class'    => 'custom-logo',
		) )
	);
	return $html;
}


/**
 * Build a background-gradient style for CSS
 *
 * @param $grad_bg_color_1      hex color value
 * @param $grad_bg_color_2      hex color value
 *
 * @return string       		CSS definition
 */
function clinic_pro_kirki_build_gradients( $grad_bg_color_1, $grad_bg_color_2 ) {

	$styles  = 'background:' . $grad_bg_color_1 . ';';
	$styles .= 'background:linear-gradient(to right,' . $grad_bg_color_1 . ' 0%,' . $grad_bg_color_2 . ' 100%);';

	return $styles;
}

/**
 * Build & enqueue the complete CSS for headers.
 *
 * @return void
 */
function clinic_pro_kirki_enqueue_header_gradients() {

	$grad_bg_color_1     = clinic_pro_get_option( 'clinic_pro_color_heading_gradient_left' );
	$grad_bg_color_2     = clinic_pro_get_option( 'clinic_pro_color_heading_gradient_right' );

	$css = '.entry-header {' . clinic_pro_kirki_build_gradients( $grad_bg_color_1, $grad_bg_color_2 ) . '}';
	wp_add_inline_style( 'ccfw-style', $css );

}
add_action( 'wp_enqueue_scripts', 'clinic_pro_kirki_enqueue_header_gradients', 999 );


/**
 * Load Globals
 */
require_once( CC_CORE . '/menus/wp_bootstrap_navwalker.php' );

/**
 * Load build hooks.
 */
require get_template_directory() . '/inc/structure/hooks.php';
require get_template_directory() . '/inc/structure/header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Recommend the Kirki plugin
 */
require get_template_directory() . '/inc/include-kirki.php';

/**
 * Load the Kirki Fallback class
 */
require get_template_directory() . '/inc/kirki-fallback.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Migrate from Clinic (free)
 */
add_action( 'after_switch_theme', 'clinic_pro_free_get_options' );

function clinic_pro_free_get_options() {

	/* Import Clinic free options */
	$clinic_pro_pro_free_mods = get_option( 'theme_mods_clinic' );

	if ( ! empty( $clinic_pro_pro_free_mods ) ) :

		foreach ( $clinic_pro_pro_free_mods as $clinic_pro_pro_pro_key => $clinic_pro_pro_pro_value ) :
			set_theme_mod( str_replace( 'clinic_pro_', 'clinic_pro_pro_', $clinic_pro_pro_pro_key ), $clinic_pro_pro_pro_value );
		endforeach;

	endif;
}

/**
 * Load WooCommerce Config.
 */
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


require get_template_directory() . '/inc/woocommerce/hooks.php';
require get_template_directory() . '/inc/woocommerce/functions.php';


/**
 * カスタム投稿「コラム」の月別アーカイブ出力
 */
global $my_archives_post_type;
add_filter( 'getarchives_where', 'my_getarchives_where', 10, 2 );
function my_getarchives_where( $where, $r ) {
  global $my_archives_post_type;
  if ( isset($r['post_type']) ) {
    $my_archives_post_type = $r['post_type'];
    $where = str_replace( '\'post\'', '\'' . $r['post_type'] . '\'', $where );
  } else {
    $my_archives_post_type = '';
  }
  return $where;
}
add_filter( 'get_archives_link', 'my_get_archives_link' );
function my_get_archives_link( $link_html ) {
  global $my_archives_post_type;
  if ( '' != $my_archives_post_type )
    $add_link .= '?post_type=' . $my_archives_post_type;
	$link_html = preg_replace("/href=\'(.+)\'\s/","href='$1".$add_link." '",$link_html);
 
  return $link_html;
}


/**
 * カスタム投稿「コラム」の記事一覧ウィジェット
 */

class NewEntryWidgetItem extends WP_Widget {
	function NewEntryWidgetItem() {
		parent::__construct(false, $name = 'コラム一覧');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数を取得
	   $entry_count = apply_filters( 'widget_entry_count', $instance['entry_count'] );
	   //日付を表示するを取得
	   $date_show = apply_filters( 'widget_entry_count', $instance['date_show'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_count; 
	   global $g_date_show; 
	   $g_entry_title = '最近の投稿';//表示数が設定されていない時は5にする
	   if($title_new){//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
	   $g_entry_count = 5;//表示数が設定されていない時は5にする
	   if ($entry_count) {//表示数が設定されているときは表示数をグローバル変数に代入
		 $g_entry_count = $entry_count;
	   }
	   $g_date_show = $date_show;
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php 
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 global $col_args;
			 $col_args = array(
				'post_type' => 'column',
				'posts_per_page' => $g_entry_count
			  );
			echo $args['before_widget'];
			echo $args['before_title'] . $g_entry_title . $args['after_title'];//見出し
			get_template_part('template-parts/column','list'); 
			echo $args['after_widget'];
			?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	$instance['entry_count'] = strip_tags($new_instance['entry_count']);
	$instance['date_show'] = strip_tags($new_instance['date_show']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   $entry_count = esc_attr($instance['entry_count']);
	   $date_show = esc_attr($instance['date_show']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラム一覧のタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('entry_count'); ?>">
		 <?php _e('表示数（半角数字）'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('entry_count'); ?>" name="<?php echo $this->get_field_name('entry_count'); ?>" type="number" value="<?php echo $entry_count; ?>" />
	   </p>
	   <?php //日付を表示するフォーム ?>
	   <p>
		 <input class="widefat" id="<?php echo $this->get_field_id('date_show'); ?>" name="<?php echo $this->get_field_name('date_show'); ?>" type="checkbox" value="1" <?php checked( $instance['date_show'], 1 ); ?>/>
		 <label for="<?php echo $this->get_field_id('date_show'); ?>">
		 <?php _e('投稿日を表示しますか ?'); ?>
		 </label>
	   </p>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetItem");'));


/**
 * カスタム投稿「コラム」のアーカイブウィジェット
 */

class NewEntryWidgetarchive extends WP_Widget {
	function NewEntryWidgetarchive() {
		parent::__construct(false, $name = 'コラムアーカイブ');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_title;
	   $g_entry_title = 'アーカイブ';//表示数が設定されていない時は5にする
	   if ($title_new) {//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php 
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 get_template_part('template-parts/column','archive'); ?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラムアーカイブのタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetarchive");'));


/**
 * カスタム投稿「コラム」のカテゴリウィジェット
 */

class NewEntryWidgetcategory extends WP_Widget {
	function NewEntryWidgetcategory() {
		parent::__construct(false, $name = 'コラムカテゴリ');//ウィジェット名
   }
   function widget($args, $instance) {
	   extract( $args );
	   //タイトル名を取得
	   $title_new = apply_filters( 'widget_title_new', $instance['title_new'] );
	   //表示数をグローバル変数に格納
	   //後で使用するテンプレートファイルへの受け渡しよ
	   //（もうちょっとスマートな方法があるのかも）
	   global $g_entry_title; 
	   $g_entry_title = 'カテゴリー';//表示数が設定されていない時は5にする
	   if ($title_new) {//表示数が設定されているときは表示数をグローバル変数に代入
		$g_entry_title = $title_new;
	   }
		?>
		 <?php //classにwidgetと一意となるクラス名を追加する ?>
		   <?php 
			 //コラム一覧表示用の処理を書くところだけど
			 //コード量も多く、インデントが深くなり読みづらくなるので
			 //テンプレートファイル側に書く
			 get_template_part('template-parts/column','category'); ?>
	   <?php
   }
   function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title_new'] = strip_tags($new_instance['title_new']);
	   return $instance;
   }
   function form($instance) {
	   $title_new = esc_attr($instance['title_new']);
	   ?>
	   <?php //タイトル入力フォーム ?>
	   <p>
		 <label for="<?php echo $this->get_field_id('title_new'); ?>">
		 <?php _e('コラムアーカイブのタイトル'); ?>
		 </label>
		 <input class="widefat" id="<?php echo $this->get_field_id('title_new'); ?>" name="<?php echo $this->get_field_name('title_new'); ?>" type="text" value="<?php echo $title_new; ?>" />
	   </p>
	   <?php //表示数入力フォーム ?>
	   <?php
   }
}
add_action('widgets_init', create_function('', 'return register_widget("NewEntryWidgetcategory");'));
