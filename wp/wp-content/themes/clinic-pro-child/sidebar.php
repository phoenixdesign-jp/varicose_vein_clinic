<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package clinic-pro
 */

if ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-column' ) ) {
	return;
}
?>

<div id="secondary" class="ccfw-default-sidebar widget-area" role="complementary">
	<?php 
	$pst_typ = get_query_var('post_type');
	if(is_singular('column') || is_tax('column_category') || $pst_typ == 'column' ):
	dynamic_sidebar( 'sidebar-column' );
	else:
	dynamic_sidebar( 'sidebar-1' );
	endif; ?>
</div><!-- #secondary -->
