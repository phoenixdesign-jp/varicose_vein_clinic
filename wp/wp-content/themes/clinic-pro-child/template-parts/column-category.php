<?php global $g_entry_title; ?>
<aside class="widget">
<h4 class="widget-title"><?php echo $g_entry_title; ?></h4>
<ul>
<?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'column_category')); ?>
</ul>
</aside>