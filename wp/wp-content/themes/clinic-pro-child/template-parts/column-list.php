<?php 
global $col_args;
global $g_date_show; 
?>
<?php $the_query = new WP_Query( $col_args );
if ( $the_query->have_posts() ) : ?>
    <?php /* Start the Loop */ ?>
    <ul>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        <?php if($g_date_show): ?>
            <span class="post-date"><?php echo get_the_date('Y年m月d日'); ?></span>
        <?php endif; ?>
        </li>
    <?php endwhile; ?>
    </ul>
    <?php clinic_pro_pagination(); ?>
<?php else : ?>
    <?php get_template_part( 'template-parts/content', 'none' ); ?>
<?php endif; 
wp_reset_postdata();
?>