$(function(){
  function scriptSwitch(){
    var windowInnerWidth = window.innerWidth;
    
    if (windowInnerWidth < 768) {
      // SP
      $('.p-nav__list__hasChildren > a,.p-nav__list__hasChildren__children__hasGrandChildren > a').on('click',function(e){
        $(this).next('ul').toggle();
        $(this).parent().toggleClass('is-open');
        e.preventDefault();
      });

      $('.p-nav__hamburger').on('click',function(e){
        $(this).next('ul').stop().slideToggle('fast');
        e.preventDefault();
      })
    } else if(windowInnerWidth < 1200) {

    } else {
      // PC
      var header = $('#header');

      //スクロールが100に達したらボタン表示
      $(window).scroll(function () {
          if ($(this).scrollTop() > 200) {
          //ボタンの表示方法
              header.addClass('is-sticky');
          } else {
          //ボタンの非表示方法
              header.removeClass('is-sticky');
          }
      });

      var headerHight = 57; //ヘッダの高さ
      $('a[href^="#"]').click(function(){
        var href= $(this).attr("href");
          var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top-headerHight; //ヘッダの高さ分位置をずらす
        $("html, body").animate({scrollTop:position}, 550, "swing");
            return false;
      });

      var topBtn = $('#backToTop');
      
      topBtn.hide();
      //スクロールが100に達したらボタン表示
      $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
          //ボタンの表示方法
              topBtn.fadeIn();
          } else {
          //ボタンの非表示方法
              topBtn.fadeOut();
          }
      });
      //スクロールしてトップ
      topBtn.click(function () {
          $('body,  html').animate({
              scrollTop: 0
          },   500);
          return false;
      });
    }
    }

  scriptSwitch();

  var timer = false;
  $(window).on('resize',function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      scriptSwitch();
    }, 200);
  });

  // 下肢静脈瘤の種類

  var varixToggle = $('.p-varixType__toggle');

  varixToggle.on('click',function(){
    $(this).next().stop().slideToggle('fast');
    $(this).toggleClass('is-open');
  });

  // 静脈瘤は2人に1人発症

  $(".count").on('inview',function(){
    var countElm = $('.count'),
        countSpeed = 30;
    
        countElm.each(function(){
            var self = $(this),
            countMax = self.attr('data-num'),
            thisCount = self.text(),
            countTimer;

            function timer(){
                countTimer = setInterval(function(){
                    var countNext = thisCount++;
                    self.text(countNext);

                    if(countNext == countMax){
                        clearInterval(countTimer);
                    }
                },countSpeed);
            }
            timer();
        });
  })

});